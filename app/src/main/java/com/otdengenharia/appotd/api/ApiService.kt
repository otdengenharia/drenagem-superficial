package com.otdengenharia.appotd.api

import com.otdengenharia.appotd.model.ApiDrenagemSuperficial
import com.otdengenharia.appotd.model.ApiInfo
import com.otdengenharia.appotd.model.ApiUsuario
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    @GET("info/readInfoUsuario.php")
    suspend fun getInfoUsuarios() : List<ApiUsuario>

    @GET("info/readInfoCampanha.php")
    suspend fun getInfoCampanhas(@Query("usuario_id") id : Long, @Query("disciplina") disciplina : String) : List<ApiInfo>

    @GET("ds/read.php")
    suspend fun getDispositivesDs(@Query("campanha_id") campanhaId : Long) : List<ApiDrenagemSuperficial>

    @Headers( "Content-Type: application/json" )
    @POST("ds/write.php")
    suspend fun sendDispositiveDs(@Body requestBody: RequestBody) : Response<ResponseBody>

    @FormUrlEncoded
    @POST("ds/uploadImage.php")
    suspend fun uploadImage(@Field("name") name : String, @Field("image") image : String, @Field("cod_auto") codAuto: Long, @Field("campanha_id") campanhaId: Long) : Response<ResponseBody>
}