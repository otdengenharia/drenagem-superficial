package com.otdengenharia.appotd.api

import com.otdengenharia.appotd.model.ApiDrenagemSuperficial
import com.otdengenharia.appotd.model.ApiInfo
import com.otdengenharia.appotd.model.ApiUsuario
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response

interface ApiHelper {
    suspend fun getInfoUsuarios() : List<ApiUsuario>

    suspend fun getInfoCampanhas(id: Long, disciplina: String): List<ApiInfo>

    suspend fun getDispositivesDs(campanhaId : Long) : List<ApiDrenagemSuperficial>

    suspend fun sendDispositiveDs(requestBody: RequestBody) : Response<ResponseBody>

    suspend fun uploadImage(name : String,image : String, codAuto : Long, campanhaId: Long) : Response<ResponseBody>

}