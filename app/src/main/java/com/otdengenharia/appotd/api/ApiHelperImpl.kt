package com.otdengenharia.appotd.api

import com.otdengenharia.appotd.model.ApiDrenagemSuperficial
import com.otdengenharia.appotd.model.ApiInfo
import com.otdengenharia.appotd.model.ApiUsuario
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response

class ApiHelperImpl(private val apiService: ApiService) : ApiHelper {
    override suspend fun getInfoUsuarios(): List<ApiUsuario> {
        return apiService.getInfoUsuarios()
    }

    override suspend fun getInfoCampanhas(id: Long, disciplina: String): List<ApiInfo> {
        return apiService.getInfoCampanhas(id, disciplina)
    }

    override suspend fun getDispositivesDs(campanhaId: Long): List<ApiDrenagemSuperficial> {
        return apiService.getDispositivesDs(campanhaId)
    }

    override suspend fun sendDispositiveDs(requestBody: RequestBody): Response<ResponseBody> {
        return apiService.sendDispositiveDs(requestBody)
    }

    override suspend fun uploadImage(
        name: String,
        image: String,
        codAuto: Long,
        campanhaId: Long
    ): Response<ResponseBody> {
        return apiService.uploadImage(name, image, codAuto, campanhaId)
    }
}

