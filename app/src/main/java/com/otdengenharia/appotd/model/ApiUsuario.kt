package com.otdengenharia.appotd.model

import com.otdengenharia.appotd.local.entity.Usuario

data class ApiUsuario(
    var id: Int,
    var nome: String,
    var email: String,
    var senha: String,
    var nivelAcesso: String,
    var status: String
) {
    fun toEntity(): Usuario {
        return Usuario(
            this.id,
            this.nome,
            this.email,
            this.senha,
            this.nivelAcesso,
            this.status
        )
    }
}