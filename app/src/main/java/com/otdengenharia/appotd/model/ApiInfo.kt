package com.otdengenharia.appotd.model

import com.otdengenharia.appotd.local.entity.Info

data class ApiInfo(
    var id: Int,
    var usuarioId: Int,
    var campanhaId: Int,
    var concessionariaId: Int,
    var nomeConcessionaria: String,
    var disciplina: String,
    var dataInicio: String,
    var dataFinal: String,
    var logo: String
) {
    fun toEntity(): Info {
        return Info(
            this.id,
            this.usuarioId,
            this.campanhaId,
            this.concessionariaId,
            this.nomeConcessionaria,
            this.disciplina,
            this.dataInicio,
            this.dataFinal,
            this.logo
        )
    }
}