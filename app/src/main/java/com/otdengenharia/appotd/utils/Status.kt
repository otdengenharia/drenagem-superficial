package com.otdengenharia.appotd.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}