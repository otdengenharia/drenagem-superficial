package com.otdengenharia.appotd.utils

enum class Avaliado {
    OK,
    ABERTO,
    PENDENTE
}