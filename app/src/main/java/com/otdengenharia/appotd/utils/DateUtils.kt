package com.otdengenharia.appotd.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    private fun getDate(): Date {
        return Calendar.getInstance().time
    }

    fun getFormattedDate(): String {
        val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        return dateFormat.format(getDate())
    }
}