package com.otdengenharia.appotd.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import java.io.*


object ImageUtils {
    fun uriToBitmap(imageUri : Uri, context : Context) : Bitmap{
        val imageBitmap: Bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            ImageDecoder.decodeBitmap(
                ImageDecoder.createSource(
                    context.contentResolver,
                    imageUri
                )
            )
        } else {
            MediaStore.Images.Media.getBitmap(context.contentResolver, imageUri)
        }
        return imageBitmap
    }

    fun convertToBitmap(encodedString: String): Bitmap {
        val imageBytes = Base64.decode(encodedString, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
    }

    fun convertToString(bitmap: Bitmap): String? {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val imgByte: ByteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(imgByte, Base64.DEFAULT)
    }

    fun readfile(fileName : String) : Bitmap? {
        val path = "/storage/emulated/0/Download/$fileName"
        return BitmapFactory.decodeFile(path) ?: null
    }

    fun storeImage(imageData: Bitmap, filename: String, uri: Uri): Boolean {
        // get path to external storage (SD card)
        var sdIconStorageDir: File? = null
        sdIconStorageDir = File(
            uri.toString()
        )
        // create storage directories, if they don't exist
        if (!sdIconStorageDir.exists()) {
            sdIconStorageDir.mkdirs()
        }
        try {
            val filePath: String =
                sdIconStorageDir.toString() + File.separator.toString() + filename
            val fileOutputStream = FileOutputStream(filePath)
            val bos = BufferedOutputStream(fileOutputStream)
            //Toast.makeText(m_cont, "Image Saved at----" + filePath, Toast.LENGTH_LONG).show();
            // choose another format if PNG doesn't suit you
            imageData.compress(Bitmap.CompressFormat.JPEG, 100, bos)
            bos.flush()
            bos.close()
        } catch (e: FileNotFoundException) {
            Log.w("TAG", "Error saving image file: " + e.message)
            return false
        } catch (e: IOException) {
            Log.w("TAG", "Error saving image file: " + e.message)
            return false
        }
        return true
    }
}