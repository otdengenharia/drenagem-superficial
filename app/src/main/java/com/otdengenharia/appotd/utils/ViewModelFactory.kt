package com.otdengenharia.appotd.utils

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.otdengenharia.appotd.api.ApiHelper
import com.otdengenharia.appotd.local.*
import com.otdengenharia.appotd.local.db.DatabaseHelper

class ViewModelFactory(private val apiHelper: ApiHelper, private val dbHelper: DatabaseHelper) :
    ViewModelProvider.Factory {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ListaDispositivosDsViewModel::class.java)) {
            return ListaDispositivosDsViewModel(apiHelper, dbHelper) as T
        }
        else if (modelClass.isAssignableFrom(FormularioDispositivoDsViewModel::class.java)) {
            return FormularioDispositivoDsViewModel(dbHelper) as T
        }
        else if (modelClass.isAssignableFrom(FotoDsViewModel::class.java)) {
            return FotoDsViewModel(dbHelper) as T
        }
        else if (modelClass.isAssignableFrom(SplashScreenViewModel::class.java)) {
            return SplashScreenViewModel(apiHelper, dbHelper) as T
        }
        else if (modelClass.isAssignableFrom(InfoViewModel::class.java)) {
            return InfoViewModel(apiHelper, dbHelper) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}