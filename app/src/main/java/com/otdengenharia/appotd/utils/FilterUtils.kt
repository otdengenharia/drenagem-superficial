package com.otdengenharia.appotd.utils

import android.util.Log
import com.otdengenharia.appotd.local.entity.DrenagemSuperficial

object FilterUtils {

    fun filter(
        dispositives: ArrayList<DrenagemSuperficial>,
        km: List<Float>,
        segmento: List<Float>,
        checkedChipIdsElemento: List<String>,
        checkedChipsSentido: List<String>,
        checkedChipsStatus: List<String>
    ): ArrayList<DrenagemSuperficial> {
        var result: ArrayList<DrenagemSuperficial> = dispositives
        result = filterKm(result, km)
        result = filterSegmento(result, segmento)
        result = filterElemento(checkedChipIdsElemento, result)
        result = filterSentido(checkedChipsSentido, result)
        result = filterStatus(checkedChipsStatus, result)
        return result
    }

    private fun filterStatus(
        checkedChipsStatus: List<String>,
        result: ArrayList<DrenagemSuperficial>
    ): ArrayList<DrenagemSuperficial> {
        var result1 = result
        if (checkedChipsStatus.isNotEmpty()) {
            result1 = result1.filter {
                it.avaliado.replace(" ","") in checkedChipsStatus
            } as ArrayList<DrenagemSuperficial>
        }
        return result1
    }

    private fun filterSentido(
        checkedChipsSentido: List<String>,
        result: ArrayList<DrenagemSuperficial>
    ): ArrayList<DrenagemSuperficial> {
        var result1 = result
        if (checkedChipsSentido.isNotEmpty()) {
            result1 = result1.filter {
                it.sentido.replace(" ","") in checkedChipsSentido
            } as ArrayList<DrenagemSuperficial>
        }
        return result1
    }

    private fun filterElemento(
        checkedChipIdsElemento: List<String>,
        result: ArrayList<DrenagemSuperficial>
    ): ArrayList<DrenagemSuperficial> {
        var filteredList = ArrayList<DrenagemSuperficial>()
        if (checkedChipIdsElemento.isNotEmpty()) {
            result.forEach {
                if (checkedChipIdsElemento.contains(it.elemento.replace(" ",""))) {
                    filteredList.add(it)
                }
            }
            return filteredList
        }
        return result
    }

    private fun filterKm(
        result: ArrayList<DrenagemSuperficial>,
        km: List<Float>
    ) = result.filter {
        it.trechoKm.toInt() >= km.first().toFloat().toInt() && it.trechoKm.toInt() <= km.last()
            .toFloat().toInt()
    } as ArrayList<DrenagemSuperficial>

    private fun filterSegmento(
        result: ArrayList<DrenagemSuperficial>,
        segmento: List<Float>
    ) = result.filter {
        it.segmento >= segmento.first().toInt() && it.segmento <= segmento.last()
            .toInt()

    } as ArrayList<DrenagemSuperficial>

}