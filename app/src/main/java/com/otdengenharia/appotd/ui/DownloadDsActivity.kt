package com.otdengenharia.appotd.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.otdengenharia.appotd.R
import com.otdengenharia.appotd.databinding.ActivityDownloadDsBinding

class DownloadDsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDownloadDsBinding

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDownloadDsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setupToolbar()

        binding.activityDownloadDsBtn.setOnClickListener {
            val segmento = binding.activityDownloadDsTextSegmento.text.toString()

            val intent = Intent(
                this@DownloadDsActivity,
                ListaDispositivosDsActivity::class.java
            ).apply {
                putExtra("segmento", segmento)
            }
            setResult(201, intent)
            finish()
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Download Fotos Referência"
    }

}