package com.otdengenharia.appotd.ui

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.otdengenharia.appotd.R
import com.otdengenharia.appotd.api.ApiHelperImpl
import com.otdengenharia.appotd.api.RetrofitBuilder
import com.otdengenharia.appotd.databinding.ActivitySplashScreenBinding
import com.otdengenharia.appotd.local.SplashScreenViewModel
import com.otdengenharia.appotd.local.db.DatabaseBuilder
import com.otdengenharia.appotd.local.db.DatabaseHelperImpl
import com.otdengenharia.appotd.utils.Status
import com.otdengenharia.appotd.utils.ViewModelFactory


class SplashScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashScreenBinding
    private lateinit var viewModel: SplashScreenViewModel
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel()
        setupObserver()
        verificaSessao()
    }

    private fun verificaSessao() {
        sharedPreferences = getSharedPreferences("user_session", MODE_PRIVATE)
        val userSession: Boolean = sharedPreferences.all.isNotEmpty()

        sharedPreferences = getSharedPreferences("info_campanha_session", MODE_PRIVATE)
        val campanhaSession: Boolean = sharedPreferences.all.isNotEmpty()

        if (userSession) {
            if (campanhaSession) {
                goToActivity(
                    Intent(
                        this@SplashScreenActivity,
                        ListaDispositivosDsActivity::class.java
                    )
                )
            } else {
                goToActivity(Intent(this@SplashScreenActivity, SelecaoActivity::class.java))
            }
        } else {
            goToActivity(Intent(this@SplashScreenActivity, LoginActivity::class.java))
        }
    }

    private fun setupObserver() {
        viewModel.getInfoUsuarios().observe(this){
            when (it.status) {
                Status.SUCCESS -> {
                    binding.activitySplashProgressbar.visibility = View.INVISIBLE
                    binding.activitySplashMsg.text = getString(R.string.msg_ataulizacao_ok)
                }
                Status.LOADING -> {
                    binding.activitySplashProgressbar.visibility = View.VISIBLE
                    binding.activitySplashMsg.text = getString(R.string.msg_atualizacao_loading)
                }
                Status.ERROR -> {
                    binding.activitySplashProgressbar.visibility = View.INVISIBLE
                    binding.activitySplashMsg.text = getString(R.string.msg_atualizacao_erro)
                }
            }
        }
    }

    private fun goToActivity(intent: Intent) {
        startActivity(intent)
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService),
                DatabaseHelperImpl(DatabaseBuilder.getInstance(applicationContext))
            )
        )[SplashScreenViewModel::class.java]
    }
}