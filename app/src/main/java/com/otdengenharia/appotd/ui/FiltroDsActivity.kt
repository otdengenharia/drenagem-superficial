package com.otdengenharia.appotd.ui

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.chip.Chip
import com.otdengenharia.appotd.R
import com.otdengenharia.appotd.databinding.ActivityFiltroDsBinding

class FiltroDsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFiltroDsBinding
    private lateinit var sharedPreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFiltroDsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setupToolbar()
        sharedPreferences = getPreferences(MODE_PRIVATE)

    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Filtros"
    }

    override fun onResume() {
        super.onResume()

        binding.activityFiltroDsBtnVerResultado.setOnClickListener {
            val checkedChipIdsElemento =
                getChipList(binding.activityFiltroDsChipGroupElemento.checkedChipIds)
            val checkedChipsSentido =
                getChipList(binding.activityFiltroDsChipGroupSentido.checkedChipIds)
            val checkedChipsStatus =
                getChipList(binding.activityFiltroDsChipGroupStatus.checkedChipIds)

            val km = binding.activityFiltroDsSliderKm.values
            val segmento = binding.activityFiltroDsSliderSegmento.values


            Log.i("km", km.toString())
            Log.i("segmento", segmento.toString())
            Log.i("elemento", checkedChipIdsElemento.toString())
            Log.i("sentido", checkedChipsSentido.toString())
            Log.i("status", checkedChipsStatus.toString())

                val intent = Intent(
                    this@FiltroDsActivity,
                    ListaDispositivosDsActivity::class.java
                ).apply {
                    putStringArrayListExtra("km", km as ArrayList<String>)
                    putStringArrayListExtra("segmento", segmento as ArrayList<String>)
                    putStringArrayListExtra("elemento", checkedChipIdsElemento as ArrayList<String>)
                    putStringArrayListExtra("sentido", checkedChipsSentido as ArrayList<String>)
                    putStringArrayListExtra("status", checkedChipsStatus as ArrayList<String>)
                }
                setResult(200, intent)
                finish()
        }
    }

    private fun getChipList(checkedChips: MutableList<Int>): List<String> {
        val listCheckedChipByGroup = mutableListOf<String>()
        checkedChips.forEach {
            val chip = findViewById<Chip>(it)
            listCheckedChipByGroup.add(chip.text.toString())
        }
        return listCheckedChipByGroup
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_limpar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.limpar_filtro -> {
                binding.activityFiltroDsChipGroupElemento.clearCheck()
                binding.activityFiltroDsChipGroupSentido.clearCheck()
                binding.activityFiltroDsChipGroupStatus.clearCheck()

                if (sharedPreferences.all.isNotEmpty()){
                    val edit = sharedPreferences.edit()
                    edit.clear().apply()
                }
                Toast.makeText(this, "Limpeza de Filtro Concluída.", Toast.LENGTH_LONG).show()

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}