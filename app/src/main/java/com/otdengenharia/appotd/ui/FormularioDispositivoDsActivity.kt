package com.otdengenharia.appotd.ui

import android.Manifest
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.otdengenharia.appotd.R
import com.otdengenharia.appotd.api.ApiHelperImpl
import com.otdengenharia.appotd.api.RetrofitBuilder
import com.otdengenharia.appotd.databinding.ActivityFormularioDispositivoDsBinding
import com.otdengenharia.appotd.local.FormularioDispositivoDsViewModel
import com.otdengenharia.appotd.local.db.DatabaseBuilder
import com.otdengenharia.appotd.local.db.DatabaseHelperImpl
import com.otdengenharia.appotd.local.entity.DrenagemSuperficial
import com.otdengenharia.appotd.utils.*


class FormularioDispositivoDsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFormularioDispositivoDsBinding
    private var dispositivo: DrenagemSuperficial? = null
    private lateinit var viewModel: FormularioDispositivoDsViewModel
    private var codAuto: Long = 0L
    private var identificacao: String = ""
    private var foto1: String? = null
    private var foto2: String? = null
    private lateinit var gpsTracker: GpsTracker
    private lateinit var sharedPreferences: SharedPreferences
    private var campanhaId : Long = 0L


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormularioDispositivoDsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setupToolbar()
        solicitaPermissaoLocation()
        checkboxDinamicoDiagnostico()
        setupViewModel()
        configuraSpinner()

        sharedPreferences = getSharedPreferences("user_session", MODE_PRIVATE)
        campanhaId = sharedPreferences.getLong("id_campanha", 0L)


        binding.activityFormularioDispositivoDsBtnCoordenadaInicial.setOnClickListener {
            getLocation("coordenada inicial")
        }

        binding.activityFormularioDispositivoDsBtnCoordenadaFinal.setOnClickListener {
            getLocation("coordenada final")
        }

        binding.activityFormularioDispositivoDsBtnCamera.setOnClickListener {
            val intent = Intent(
                this@FormularioDispositivoDsActivity,
                FotoDsActivity::class.java
            ).apply {
                putExtra("dispositiveDs", dispositivo)
            }
            startActivity(intent)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        return
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Drenagem Superficial"
    }

    private fun checkboxDinamicoDiagnostico() {
        // To listen for a checkbox's checked/unchecked state changes
        binding.activityFormularioDispositivoDsCheckboxOk.setOnCheckedChangeListener { _, isChecked ->
            // Responds to checkbox being checked/unchecked
            if (isChecked) {
                binding.activityFormularioDispositivoDsCheckboxLimpeza.isChecked = false
                binding.activityFormularioDispositivoDsCheckboxReparar.isChecked = false

                binding.activityFormularioDispositivoDsLimpeza.setText("0.0")
                binding.activityFormularioDispositivoDsReparar.setText("0.0")

                binding.activityFormularioDispositivoDsTextinputlayoutExtensaoLimpeza.visibility =
                    GONE
                binding.activityFormularioDispositivoDsTextinputlayoutExtensaoReparar.visibility =
                    GONE
            }
        }

        binding.activityFormularioDispositivoDsCheckboxLimpeza.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoDsLimpeza.setText("0.0")
                binding.activityFormularioDispositivoDsTextinputlayoutExtensaoLimpeza.visibility =
                    View.VISIBLE
                binding.activityFormularioDispositivoDsCheckboxOk.isChecked = false
            } else {
                binding.activityFormularioDispositivoDsTextinputlayoutExtensaoLimpeza.visibility =
                    GONE
            }
        }

        binding.activityFormularioDispositivoDsCheckboxReparar.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoDsReparar.setText("0.0")
                binding.activityFormularioDispositivoDsTextinputlayoutExtensaoReparar.visibility =
                    View.VISIBLE
                binding.activityFormularioDispositivoDsCheckboxOk.isChecked = false
            } else {
                binding.activityFormularioDispositivoDsTextinputlayoutExtensaoReparar.visibility =
                    GONE
            }
        }
    }

    private fun solicitaPermissaoLocation() {
        try {
            if (ContextCompat.checkSelfPermission(
                    applicationContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    101
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()

        if(intent.hasExtra("codAuto")){
            saveSharedPreferences()
        } else {
            sharedPreferences = getPreferences(MODE_PRIVATE)
            codAuto = sharedPreferences.getLong("codAuto", 0L)
        }

        if (codAuto==0L){
            supportActionBar?.title = "Cadastrar Dispositivo (DS)"
            binding.activityFormularioDispositivoDsIdentificacaoDispositivo.visibility = GONE
            binding.activityFormularioDispositivoDsFoto1.visibility = GONE
            binding.activityFormularioDispositivoDsFoto2.visibility = GONE
            binding.activityFormularioDispositivoDsBtnCamera.visibility = GONE
        } else if (codAuto != 0L) {
            supportActionBar?.title = "Alterar Dispositivo (DS)"
            tentaPreencherFormulario()
            configuraSpinner()
            binding.activityFormularioDispositivoDsIdentificacaoDispositivo.text =
                dispositivo?.identificacao
            binding.activityFormularioDispositivoDsSegmento.isEnabled = false
            verificaFotoAntiga()
        }

        binding.activityFormularioDispositivoDsBtnSalvar.setOnClickListener {
            if (isEmpty(binding.activityFormularioDispositivoDsKm.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Km")
            } else if (isEmpty(binding.activityFormularioDispositivoDsKmFinal.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Km Final")
            } else if (isEmpty(binding.activityFormularioDispositivoDsSegmento.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Segmento")
            } else if (isEmpty(binding.activityFormularioDispositivoDsComprimento.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Comprimento")
            } else if (isEmpty(binding.activityFormularioDispositivoDsAltura.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Altura")
            } else if (isEmpty(binding.activityFormularioDispositivoDsLargura.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Largura")
            } else if (!binding.activityFormularioDispositivoDsCheckboxOk.isChecked &&
                !binding.activityFormularioDispositivoDsCheckboxLimpeza.isChecked &&
                !binding.activityFormularioDispositivoDsCheckboxReparar.isChecked
            ) {
                toastPreencherCampoPendente("Marque pelo menos uma opção de Diagnóstico")
            } else {
                setIdentificacao()
                salvarDispositivo()
                pegaDispositivoSalvo()
                rolaParaTopoDaPagina()
                binding.activityFormularioDispositivoDsIdentificacaoDispositivo.text = identificacao
                binding.activityFormularioDispositivoDsIdentificacaoDispositivo.visibility =
                    View.VISIBLE
                binding.activityFormularioDispositivoDsBtnCamera.visibility = View.VISIBLE
            }
        }
    }

    private fun pegaDispositivoSalvo() {
        viewModel.getMaxCodAuto().apply {
            dispositivo = viewModel.getDispositiveDs(codAuto)
        }
    }

    private fun verificaFotoAntiga() {
        if (dispositivo?.foto1 == null && dispositivo?.foto2 == null) {
            binding.activityFormularioDispositivoDsFoto1.visibility = GONE
            binding.activityFormularioDispositivoDsFoto2.visibility = GONE
        }
    }

    private fun rolaParaTopoDaPagina() {
        binding.activityFormularioDispositivoDsScrollview.fullScroll(View.FOCUS_UP)
    }

    private fun toastPreencherCampoPendente(msg: String) {
        val toast = Toast.makeText(this, msg, Toast.LENGTH_LONG)
        toast.show()
    }

    private fun isEmpty(string: String): Boolean {
        return (string == "" || string.trim() == "")
    }

    private fun getLocation(param: String) {
        gpsTracker = GpsTracker(this)
        if (gpsTracker.canGetLocation()) {
            val latitude = gpsTracker.latitude
            val longitude = gpsTracker.longitude

            if (param == "coordenada inicial") {
                binding.activityFormularioDispositivoDsLatitude1.setText(latitude.toString())
                binding.activityFormularioDispositivoDsLongitude1.setText(longitude.toString())
            } else if (param == "coordenada final") {
                binding.activityFormularioDispositivoDsLatitude2.setText(latitude.toString())
                binding.activityFormularioDispositivoDsLongitude2.setText(longitude.toString())
            }
        } else {
            gpsTracker.showSettingsAlert()
        }
    }

    private fun getUserId(): Long {
        sharedPreferences = getSharedPreferences("user_session", MODE_PRIVATE)
        return sharedPreferences.getLong("id", 0L)
    }

    private fun salvarDispositivo() {
        val disp = DrenagemSuperficial(
            codAuto,
            identificacao,
            binding.activityFormularioDispositivoDsElemento.text.toString(),
            binding.activityFormularioDispositivoDsKm.text.toString(),
            binding.activityFormularioDispositivoDsLatitude1.text.toString(),
            binding.activityFormularioDispositivoDsLongitude1.text.toString(),
            binding.activityFormularioDispositivoDsLatitude2.text.toString(),
            binding.activityFormularioDispositivoDsLongitude2.text.toString(),
            binding.activityFormularioDispositivoDsKm.text.toString().substring(0, 3),
            binding.activityFormularioDispositivoDsKmFinal.text.toString(),
            binding.activityFormularioDispositivoDsEstado.text.toString(),
            binding.activityFormularioDispositivoDsRodovia.text.toString(),
            binding.activityFormularioDispositivoDsSentido.text.toString(),
            binding.activityFormularioDispositivoDsCorteAterro.text.toString(),
            binding.activityFormularioDispositivoDsMaterial.text.toString(),
            binding.activityFormularioDispositivoDsComprimento.text.toString().toDouble(),
            binding.activityFormularioDispositivoDsLargura.text.toString().toDouble(),
            binding.activityFormularioDispositivoDsAltura.text.toString().toDouble(),
            binding.activityFormularioDispositivoDsForma.text.toString(),
            binding.activityFormularioDispositivoDsAmbiente.text.toString(),
            binding.activityFormularioDispositivoDsTipo.text.toString(),
            binding.activityFormularioDispositivoDsCheckboxOk.isChecked.toString(),
            binding.activityFormularioDispositivoDsCheckboxReparar.isChecked.toString(),
            binding.activityFormularioDispositivoDsReparar.text.toString().toDouble(),
            binding.activityFormularioDispositivoDsCheckboxLimpeza.isChecked.toString(),
            binding.activityFormularioDispositivoDsLimpeza.text.toString().toDouble(),
            null,
            binding.activityFormularioDispositivoDsObservacao.text.toString(),
            getUserId().toString(),
            DateUtils.getFormattedDate(),
            binding.activityFormularioDispositivoDsLote.text.toString(),
            foto1,
            foto2,
            binding.activityFormularioDispositivoDsSegmento.text.toString().toInt(),
            avaliado = (if (dispositivo?.avaliado == Avaliado.PENDENTE.toString()) Avaliado.ABERTO.toString() else Avaliado.OK.toString()),
            "N",
            campanhaId
        )

        Log.i("add", disp.codAuto.toString())
        viewModel.insertDispositiveDs(disp)

        Toast.makeText(
            this@FormularioDispositivoDsActivity,
            "Diagnóstico realizado com sucesso!",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun setIdentificacao() {
        if (dispositivo == null) {
            val siglaElemento = getSiglaElemento()
            val numRodovia = getNumRodovia()
            val siglaSentido = getSiglaSentido()
            val estado = binding.activityFormularioDispositivoDsEstado.text.toString()
            val km = binding.activityFormularioDispositivoDsKm.text.toString()
            val identificacaoBase = "$siglaElemento $numRodovia $estado $km $siglaSentido"
            val numElementos = viewModel.verifyIdentification(identificacaoBase) + 1

            identificacao =
                "$siglaElemento $numRodovia $estado $km $siglaSentido $numElementos"

            codAuto = (viewModel.getMaxCodAuto() + 1)
        } else {
            identificacao = dispositivo?.identificacao.toString()
            codAuto = dispositivo?.codAuto!!.toLong()
        }
    }

    private fun getNumRodovia(): String {
        var numRodovia = binding.activityFormularioDispositivoDsRodovia.text.toString()
        numRodovia = numRodovia.substring(numRodovia.length - 3, numRodovia.length)
        return numRodovia
    }

    private fun getSiglaSentido() =
        binding.activityFormularioDispositivoDsSentido.text.toString()[0]

    private fun getSiglaElemento(): String {
        var elemento = binding.activityFormularioDispositivoDsElemento.text.toString()
        when (elemento) {
            "CAIXA" -> {
                elemento = "CX"
            }
            "CANALETA" -> {
                elemento = "CA"
            }
            "DESCIDA D\'ÁGUA" -> {
                elemento = "DA"
            }
            "DISSIPADOR" -> {
                elemento = "DI"
            }
            "MEIO FIO" -> {
                elemento = "MF"
            }
            "SARJETA" -> {
                elemento = "SJ"
            }
            "VALETA" -> {
                elemento = "VA"
            }
        }
        return elemento
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService),
                DatabaseHelperImpl(DatabaseBuilder.getInstance(applicationContext))
            )
        )[FormularioDispositivoDsViewModel::class.java]
    }

    private fun tentaPreencherFormulario() {
        val id = sharedPreferences.getLong("codAuto", 0L)
        dispositivo = viewModel.getDispositiveDs(id)
        dispositivo?.let {
            identificacao = it.identificacao
            foto1 = it.foto1
            foto2 = it.foto2
        }

        binding.activityFormularioDispositivoDsKm.setText(dispositivo?.km)
        binding.activityFormularioDispositivoDsKmFinal.setText(dispositivo?.kmFinal)
        binding.activityFormularioDispositivoDsEstado.setText(dispositivo?.estado)
        binding.activityFormularioDispositivoDsRodovia.setText(dispositivo?.rodovia)
        binding.activityFormularioDispositivoDsLote.setText(dispositivo?.lote)
        binding.activityFormularioDispositivoDsSentido.setText(dispositivo?.sentido)
        binding.activityFormularioDispositivoDsSegmento.setText(dispositivo?.segmento.toString())
        binding.activityFormularioDispositivoDsLatitude1.setText(dispositivo?.latitude1)
        binding.activityFormularioDispositivoDsLatitude2.setText(dispositivo?.latitude2)
        binding.activityFormularioDispositivoDsLongitude1.setText(dispositivo?.longitude1)
        binding.activityFormularioDispositivoDsLongitude2.setText(dispositivo?.longitude2)
        binding.activityFormularioDispositivoDsElemento.setText(dispositivo?.elemento)
        binding.activityFormularioDispositivoDsMaterial.setText(dispositivo?.material)
        binding.activityFormularioDispositivoDsCorteAterro.setText(dispositivo?.corteAterro)
        binding.activityFormularioDispositivoDsAmbiente.setText(dispositivo?.ambiente)
        binding.activityFormularioDispositivoDsComprimento.setText(dispositivo?.comprimento.toString())
        binding.activityFormularioDispositivoDsAltura.setText(dispositivo?.altura.toString())
        binding.activityFormularioDispositivoDsLargura.setText(dispositivo?.largura.toString())
        binding.activityFormularioDispositivoDsForma.setText(dispositivo?.forma)
        binding.activityFormularioDispositivoDsTipo.setText(dispositivo?.tipo)
        binding.activityFormularioDispositivoDsCheckboxOk.isChecked = dispositivo!!.ok.toBoolean()
        binding.activityFormularioDispositivoDsCheckboxReparar.isChecked =
            dispositivo!!.reparar.toBoolean()
        binding.activityFormularioDispositivoDsCheckboxLimpeza.isChecked =
            dispositivo!!.limpeza.toBoolean()
        binding.activityFormularioDispositivoDsReparar.setText(dispositivo?.extensaoReparar.toString())
        binding.activityFormularioDispositivoDsLimpeza.setText(dispositivo?.extensaoLimpeza.toString())
        binding.activityFormularioDispositivoDsObservacao.setText(dispositivo?.observacao)

        if (!(dispositivo?.foto1.isNullOrBlank())) {
            if (ImageUtils.readfile(
                    dispositivo?.foto1!!
                ) != null
            ) {
                binding.activityFormularioDispositivoDsFoto1.setImageBitmap(
                    ImageUtils.readfile(
                        dispositivo?.foto1!!
                    )
                )
            }
        }

        if (!(dispositivo?.foto2.isNullOrBlank())) {
            if (ImageUtils.readfile(
                    dispositivo?.foto2!!
                ) != null
            ) {
                binding.activityFormularioDispositivoDsFoto2.setImageBitmap(
                    ImageUtils.readfile(
                        dispositivo?.foto2!!
                    )
                )
            }
        }
    }

    private fun saveSharedPreferences() {
        codAuto = intent.getLongExtra("codAuto", 0L)
        sharedPreferences = getPreferences(MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putLong("codAuto", codAuto)
        edit.apply()
    }

    private fun configuraSpinner() {
        configuraSpinnerEstado()
        configuraSpinnerRodovia()
        configuraSpinnerSentido()
        configuraSpinnerLote()
        configuraSpinnerElemento()
        configuraSpinnerMaterial()
        configuraSpinnerCorteAterro()
        configuraSpinnerAmbiente()
        configuraSpinnerForma()
    }

    private fun configuraSpinnerMaterial() {
        val materiais = resources.getStringArray(R.array.materiais)
        val arrayAdapter =
            ArrayAdapter(this@FormularioDispositivoDsActivity, R.layout.dropdown_item, materiais)
        binding.activityFormularioDispositivoDsMaterial.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerEstado() {
        val estados = resources.getStringArray(R.array.estados)
        val arrayAdapter =
            ArrayAdapter(this@FormularioDispositivoDsActivity, R.layout.dropdown_item, estados)
        binding.activityFormularioDispositivoDsEstado.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerRodovia() {
        val rodovias = resources.getStringArray(R.array.rodovias)
        val arrayAdapter =
            ArrayAdapter(this@FormularioDispositivoDsActivity, R.layout.dropdown_item, rodovias)
        binding.activityFormularioDispositivoDsRodovia.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerSentido() {
        val sentidos = resources.getStringArray(R.array.sentidos)
        val arrayAdapter =
            ArrayAdapter(this@FormularioDispositivoDsActivity, R.layout.dropdown_item, sentidos)
        binding.activityFormularioDispositivoDsSentido.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerLote() {
        val lotes = resources.getStringArray(R.array.lotes)
        val arrayAdapter =
            ArrayAdapter(this@FormularioDispositivoDsActivity, R.layout.dropdown_item, lotes)
        binding.activityFormularioDispositivoDsLote.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerElemento() {
        val elementos = resources.getStringArray(R.array.elementos)
        val arrayAdapter =
            ArrayAdapter(this@FormularioDispositivoDsActivity, R.layout.dropdown_item, elementos)
        binding.activityFormularioDispositivoDsElemento.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerCorteAterro() {
        val corteAterro = resources.getStringArray(R.array.corte_aterro)
        val arrayAdapter =
            ArrayAdapter(this@FormularioDispositivoDsActivity, R.layout.dropdown_item, corteAterro)
        binding.activityFormularioDispositivoDsCorteAterro.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerAmbiente() {
        val ambientes = resources.getStringArray(R.array.ambientes)
        val arrayAdapter =
            ArrayAdapter(this@FormularioDispositivoDsActivity, R.layout.dropdown_item, ambientes)
        binding.activityFormularioDispositivoDsAmbiente.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerForma() {
        val formas = resources.getStringArray(R.array.formas)
        val arrayAdapter =
            ArrayAdapter(this@FormularioDispositivoDsActivity, R.layout.dropdown_item, formas)
        binding.activityFormularioDispositivoDsForma.setAdapter(arrayAdapter)
    }
}