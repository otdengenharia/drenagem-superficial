package com.otdengenharia.appotd.ui

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import com.otdengenharia.appotd.R
import com.otdengenharia.appotd.databinding.ActivityInfoCampanhaBinding
import com.otdengenharia.appotd.local.entity.Usuario

class InfoCampanhaActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var binding: ActivityInfoCampanhaBinding
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navView: NavigationView
    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInfoCampanhaBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val userSession = getUserSession()
        val nome = userSession?.nome
        val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Drenagem Superficial"
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        val headerView = navView.getHeaderView(0)
        headerView.findViewById<TextView>(R.id.nav_header_colaborador).text = nome

        sharedPreferences = getSharedPreferences("info_campanha_session", MODE_PRIVATE)
        val campanhaId = sharedPreferences.getInt("id_campanha", 0)
        val nomeConcessionaria = sharedPreferences.getString("nome_concessionaria", "")
        val dataInicio = sharedPreferences.getString("data_inicio", "")
        val dataFinal = sharedPreferences.getString("data_final", "")
        val logo = sharedPreferences.getString("logo", "")

        findViewById<TextView>(R.id.content_info_campanha_nome).text = nomeConcessionaria
        findViewById<TextView>(R.id.content_info_campanha_data_inicio).text = dataInicio
        findViewById<TextView>(R.id.content_info_campanha_data_final).text = dataFinal

        if (logo != null) {
            setLogoConcessionaria(logo)
        }
    }

    private fun setLogoConcessionaria(imageName: String) {
        val logo = findViewById<ImageView>(R.id.content_info_campanha_logo)
        val resourceName = imageName.split(".png").first()
        val imageResource = resources.getIdentifier(resourceName, "drawable", packageName)
        val draw = AppCompatResources.getDrawable(this, imageResource)
        logo.setImageDrawable(draw)
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        return
    }

    private fun getUserSession(): Usuario? {
        sharedPreferences = getSharedPreferences("user_session", MODE_PRIVATE)
        return if (sharedPreferences.all.isNotEmpty()) {
            val userId = sharedPreferences.getLong("id", 0L)
            val nome = sharedPreferences.getString("nome", "")
            val email = sharedPreferences.getString("email", "")
            val status = sharedPreferences.getString("status", "")
            val nivelAcesso = sharedPreferences.getString("nivel_acesso", "")
            Usuario(userId.toInt(), nome!!, email!!, "", nivelAcesso!!, status!!)
        } else {
            null
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_menu_lista_dispositivos -> {
                goToActivity(
                    Intent(
                        this@InfoCampanhaActivity,
                        ListaDispositivosDsActivity::class.java
                    )
                )
            }
            R.id.nav_menu_info_campanha -> {

            }
            R.id.nav_menu_selecionar_campanha -> {
                goToActivity(
                    Intent(
                        this@InfoCampanhaActivity,
                        SelecaoActivity::class.java
                    )
                )
            }
            R.id.nav_menu_upload -> {

            }
            R.id.nav_menu_download -> {

            }
            R.id.nav_menu_logout -> {
                endUserSession()
                endInfoCampanhaSession()
                goToActivity(Intent(this@InfoCampanhaActivity, LoginActivity::class.java))
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun endInfoCampanhaSession() {
        sharedPreferences = getSharedPreferences("info_campanha_session", MODE_PRIVATE)
        sharedPreferences.all.clear()
    }

    private fun endUserSession() {
        sharedPreferences = getSharedPreferences("user_session", MODE_PRIVATE)
        sharedPreferences.all.clear()
    }

    private fun goToActivity(intent: Intent) {
        startActivity(intent)
    }


}