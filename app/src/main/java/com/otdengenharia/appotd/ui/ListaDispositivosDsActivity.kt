package com.otdengenharia.appotd.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.otdengenharia.appotd.R
import com.otdengenharia.appotd.api.ApiHelperImpl
import com.otdengenharia.appotd.api.RetrofitBuilder
import com.otdengenharia.appotd.databinding.ActivityListaDispositivosDsBinding
import com.otdengenharia.appotd.local.ListaDispositivosDsViewModel
import com.otdengenharia.appotd.local.db.DatabaseBuilder
import com.otdengenharia.appotd.local.db.DatabaseHelperImpl
import com.otdengenharia.appotd.local.entity.DrenagemSuperficial
import com.otdengenharia.appotd.local.entity.FotoDs
import com.otdengenharia.appotd.ui.recyclerview.adapter.RecyclerAdapterDrenagemSuperficial
import com.otdengenharia.appotd.utils.Avaliado
import com.otdengenharia.appotd.utils.FilterUtils
import com.otdengenharia.appotd.utils.Status
import com.otdengenharia.appotd.utils.ViewModelFactory


class ListaDispositivosDsActivity : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener {
    private lateinit var binding: ActivityListaDispositivosDsBinding
    private lateinit var adapter: RecyclerAdapterDrenagemSuperficial
    private lateinit var viewModel: ListaDispositivosDsViewModel
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navView: NavigationView

    private var filtro = false
    private var listaDispositivosDs: ArrayList<DrenagemSuperficial> = ArrayList()
    private var result = ArrayList<DrenagemSuperficial>()
    private var total = 0
    private var totalPhotos = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListaDispositivosDsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val nome = saveUserSession()

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Drenagem Superficial"
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        val headerView = navView.getHeaderView(0)
        headerView.findViewById<TextView>(R.id.nav_header_colaborador).text = nome

        setupViewModel()
        setupFabButton()
    }

    override fun onBackPressed() {
        return
    }

    private fun getCampanhaId(): Long {
        sharedPreferences = getSharedPreferences("info_campanha_session", MODE_PRIVATE)
        return sharedPreferences.getInt("id_campanha", 0).toLong()
    }

    private fun getNomeCampanha() : String {
        sharedPreferences = getSharedPreferences("info_campanha_session", MODE_PRIVATE)
        return sharedPreferences.getString("nome_concessionaria", "").toString()
    }

    private fun saveUserSession(): String? {
        sharedPreferences = getSharedPreferences("user_session", MODE_PRIVATE)

        val userId = sharedPreferences.getLong("id", 0L)
        val nome = sharedPreferences.getString("nome", "")
        val email = sharedPreferences.getString("email", "")
        val status = sharedPreferences.getString("status", "")
        val nivelAcesso = sharedPreferences.getString("nivel_acesso", "")

        Log.i("idUser", userId.toString())
        Log.i("nomeUser", nome.toString())
        Log.i("emailUser", email.toString())
        Log.i("statusUser", status.toString())
        Log.i("naUser", nivelAcesso.toString())
        return nome
    }

    private fun goToActivity(intent: Intent) {
        startActivity(intent)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_menu_info_campanha -> {
                goToActivity(
                    Intent(
                        this@ListaDispositivosDsActivity,
                        InfoCampanhaActivity::class.java
                    )
                )
            }
            R.id.nav_menu_selecionar_campanha -> {
                goToActivity(
                    Intent(
                        this@ListaDispositivosDsActivity,
                        SelecaoActivity::class.java
                    )
                )
            }
            R.id.nav_menu_upload -> {
                viewModel.uploadImagesAndDispositives(this, getCampanhaId())
            }
            R.id.nav_menu_download -> {
                val intent = Intent(
                    this@ListaDispositivosDsActivity,
                    DownloadDsActivity::class.java
                )
                startActivityForResult(intent, 201)
            }
            R.id.nav_menu_logout -> {
                endUserSession()
                endInfoCampanhaSession()
                goToActivity(
                    Intent(
                        this@ListaDispositivosDsActivity,
                        LoginActivity::class.java
                    )
                )
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun endInfoCampanhaSession() {
        sharedPreferences = getSharedPreferences("info_campanha_session", MODE_PRIVATE)
        sharedPreferences.all.clear()
    }

    private fun endUserSession() {
        sharedPreferences = getSharedPreferences("user_session", MODE_PRIVATE)
        sharedPreferences.all.clear()
    }

    private fun setupObserver() {
        viewModel.getDispositivesDs(getCampanhaId()).observe(this) { it ->
            when (it.status) {
                Status.SUCCESS -> {
                    findViewById<ProgressBar>(R.id.activity_lista_dispositivos_ds_progressbar).visibility =
                        View.GONE
                    it.data?.let { dispositivesDs ->
                        listaDispositivosDs = dispositivesDs as ArrayList<DrenagemSuperficial>

                        sharedPreferences = getPreferences(MODE_PRIVATE)

                        if (sharedPreferences.all.isNotEmpty()) {
                            val km = sharedPreferences.getString("km", "")?.replace("[", "")
                                ?.replace("]", "")?.split(",")
                            Log.i("km", km.toString())
                            val kmArray = ArrayList<Float>()
                            kmArray.add(km?.first()?.toFloat()!!)
                            kmArray.add(km.last().toFloat())

                            val segmento =
                                sharedPreferences.getString("segmento", "")?.replace("[", "")
                                    ?.replace("]", "")?.split(",")
                            val segmentoArray = ArrayList<Float>()
                            segmentoArray.add(segmento?.first()?.toFloat()!!)
                            segmentoArray.add(segmento.last().toFloat())

                            val elemento =
                                sharedPreferences.getString("elemento", "")?.replace("[", "")
                                    ?.replace("]", "")?.replace(" ", "")?.split(",")
                            val elementoArray = ArrayList<String>()
                            elemento?.forEach { el ->
                                if (el != "") {
                                    elementoArray.add(el)
                                }
                            }

                            val sentido =
                                sharedPreferences.getString("sentido", "")?.replace("[", "")
                                    ?.replace("]", "")?.replace(" ", "")?.split(",")
                            val sentidoArray = ArrayList<String>()
                            sentido?.forEach {
                                if (it != "") {
                                    sentidoArray.add(it)
                                }
                            }

                            val status = sharedPreferences.getString("status", "")?.replace("[", "")
                                ?.replace("]", "")?.replace(" ", "")?.split(",")
                            val statusArray = ArrayList<String>()
                            status?.forEach {
                                if (it != "") {
                                    statusArray.add(it)
                                }
                            }
                            result = FilterUtils.filter(
                                listaDispositivosDs,
                                kmArray,
                                segmentoArray,
                                elementoArray,
                                sentidoArray,
                                statusArray
                            )
                        } else {
                            result = listaDispositivosDs
                        }
                        configuraRecyclerView(
                            this,
                            result as MutableList<DrenagemSuperficial>,
                            viewModel.getAllPhotos() as MutableList<FotoDs>
                        )
                        clicaCardDispositivo()
                        preencheEstatisticasFichas()
//                        Toast.makeText(this, "Foram encontradas: " + result.size, Toast.LENGTH_LONG)
//                            .show()
                    }
                    findViewById<RecyclerView>(R.id.activity_lista_dispositivos_ds_recyclerView).visibility =
                        View.VISIBLE
                }
                Status.LOADING -> {
                    findViewById<ProgressBar>(R.id.activity_lista_dispositivos_ds_progressbar).visibility =
                        View.VISIBLE
                    findViewById<RecyclerView>(R.id.activity_lista_dispositivos_ds_recyclerView).visibility =
                        View.GONE
                }
                Status.ERROR -> {
                    //Handle Error
                    findViewById<ProgressBar>(R.id.activity_lista_dispositivos_ds_progressbar).visibility =
                        View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun setupObserverUpload() {
        totalPhotos = viewModel.counPhotosDispositivesDs()
        Log.i("totalPhotos", totalPhotos.toString())
        var countPhotos = 0
        viewModel.getResponseDispositivesDs().observe(this) {

            when (it.status) {
                Status.SUCCESS -> {
                    findViewById<ProgressBar>(R.id.activity_lista_dispositivos_ds_progressbar).visibility =
                        View.GONE
                    findViewById<RecyclerView>(R.id.activity_lista_dispositivos_ds_recyclerView).visibility =
                        View.VISIBLE
                }
                Status.LOADING -> {
                    findViewById<ProgressBar>(R.id.activity_lista_dispositivos_ds_progressbar).visibility =
                        View.VISIBLE
                    findViewById<RecyclerView>(R.id.activity_lista_dispositivos_ds_recyclerView).visibility =
                        View.GONE
                    findViewById<TextView>(R.id.activity_lista_dispositivos_ds_count).text = "$countPhotos/$totalPhotos"
                    countPhotos ++
                    Toast.makeText(this, "Upload em andamento.", Toast.LENGTH_LONG).show()
                }
                Status.ERROR -> {
                    //Handle Error
                    findViewById<ProgressBar>(R.id.activity_lista_dispositivos_ds_progressbar).visibility =
                        View.GONE
                    findViewById<TextView>(R.id.activity_lista_dispositivos_ds_count).visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setupObserverDownload() {
        var count =0
        viewModel.getPhotoDispositivesDs().observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    findViewById<ProgressBar>(R.id.activity_lista_dispositivos_ds_progressbar).visibility =
                        View.GONE
                    findViewById<RecyclerView>(R.id.activity_lista_dispositivos_ds_recyclerView).visibility =
                        View.VISIBLE
                    findViewById<TextView>(R.id.activity_lista_dispositivos_ds_count).visibility = View.GONE

                }
                Status.LOADING -> {
                    findViewById<ProgressBar>(R.id.activity_lista_dispositivos_ds_progressbar).visibility =
                        View.VISIBLE
                    findViewById<RecyclerView>(R.id.activity_lista_dispositivos_ds_recyclerView).visibility =
                        View.GONE
                    findViewById<TextView>(R.id.activity_lista_dispositivos_ds_count).visibility = View.VISIBLE
                    findViewById<TextView>(R.id.activity_lista_dispositivos_ds_count).text = "$count/$total"
                    count += 2
                }
                Status.ERROR -> {
                    //Handle Error
                    findViewById<RecyclerView>(R.id.activity_lista_dispositivos_ds_recyclerView).visibility =
                        View.GONE
                    findViewById<TextView>(R.id.activity_lista_dispositivos_ds_count).visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun preencheEstatisticasFichas() {
        val statistics = viewModel.statistics()
        findViewById<TextView>(R.id.activity_lista_dispositivos_ds_acumulado_geral_valor).text =
            statistics[Avaliado.OK.toString()].toString()
        findViewById<TextView>(R.id.activity_lista_dispositivos_ds_acumulado_diario_valor).text =
            statistics["OK_DAY"].toString()
        findViewById<TextView>(R.id.activity_lista_dispositivos_ds_em_aberto_valor).text =
            statistics[Avaliado.ABERTO.toString()].toString()
        findViewById<TextView>(R.id.activity_lista_dispositivos_ds_pendentes_valor).text =
            (statistics[Avaliado.PENDENTE.toString()]?.plus(statistics[Avaliado.ABERTO.toString()]!!)).toString()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onResume() {
        super.onResume()
        setupViewModel()
        setupObserver()
        setupObserverUpload()
        setupObserverDownload()

        configuraRecyclerView(
            this,
            result as MutableList<DrenagemSuperficial>,
            viewModel.getAllPhotos() as MutableList<FotoDs>
        )

        clicaCardDispositivo()
        preencheEstatisticasFichas()
        setupSearchView()
    }

    private fun setupSearchView() {
        findViewById<SearchView>(R.id.activity_lista_dispositivos_ds_search).setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    adapter.getFilter().filter(query)
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    adapter.getFilter().filter(newText)
                    return false
                }
            })
    }

    private fun configuraRecyclerView(
        context: Context,
        lista: MutableList<DrenagemSuperficial>,
        fotos: MutableList<FotoDs>
    ) {
        this.adapter = RecyclerAdapterDrenagemSuperficial(
            context = context,
            dispositivos = lista,
            fotos = fotos
        )
        findViewById<RecyclerView>(R.id.activity_lista_dispositivos_ds_recyclerView).adapter =
            adapter
    }

    private fun setupFabButton() {
        findViewById<FloatingActionButton>(R.id.activity_lista_dispositivos_ds_fab_add).setOnClickListener {
            if (sharedPreferences.all.isNotEmpty()) {
                val edit = sharedPreferences.edit()
                edit.clear().apply()
            }
            val intent = Intent(
                this@ListaDispositivosDsActivity,
                FormularioDispositivoDsActivity::class.java
            ).apply {
                putExtra("codAuto", 0L)
            }
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.desc -> {
                configuraRecyclerView(
                    this,
                    result.asReversed(),
                    viewModel.getAllPhotos() as MutableList<FotoDs>
                )
                true
            }
            R.id.asc -> {
                configuraRecyclerView(
                    this,
                    (result.asReversed()).asReversed(),
                    viewModel.getAllPhotos() as MutableList<FotoDs>
                )
                true
            }
            R.id.filter -> {
                val intent = Intent(
                    this@ListaDispositivosDsActivity,
                    FiltroDsActivity::class.java
                )
                startActivityForResult(intent, 200)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 200) {
            filtro = true
            val kmArray = data?.getStringArrayListExtra("km") as ArrayList<Float>
            val segmentoArray = data.getStringArrayListExtra("segmento") as ArrayList<Float>
            val elementoArray = data.getStringArrayListExtra("elemento")
            val sentidoArray = data.getStringArrayListExtra("sentido")
            val statusArray = data.getStringArrayListExtra("status")

            saveSharedPreferences(kmArray, segmentoArray, elementoArray, sentidoArray, statusArray)

            result = FilterUtils.filter(
                listaDispositivosDs,
                kmArray,
                segmentoArray,
                elementoArray!!,
                sentidoArray!!,
                statusArray!!
            )

//            Toast.makeText(this, "Foram encontradas: " + result.size, Toast.LENGTH_LONG).show()
        } else if (resultCode == 201) {
            val segmento = data?.getStringExtra("segmento")

            if (segmento != null) {
                viewModel.setSegmento(this, segmento.toInt(),getNomeCampanha().uppercase().replace(" ","_"))
                total = viewModel.countDispositivesBySegmento(segmento)
            }
        }
    }

    private fun saveSharedPreferences(
        kmArray: List<Float>,
        segmentoArray: List<Float>,
        elementoArray: List<String>?,
        sentidoArray: List<String>?,
        statusArray: List<String>?
    ) {
        sharedPreferences = getPreferences(MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString("km", kmArray.toString())
        edit.putString("segmento", segmentoArray.toString())
        edit.putString("elemento", elementoArray.toString())
        edit.putString("sentido", sentidoArray.toString())
        edit.putString("status", statusArray.toString())
        edit.apply()
    }

    private fun clicaCardDispositivo() {
        adapter.setOnItemClickListener(object :
            RecyclerAdapterDrenagemSuperficial.OnItemClickListener {
            override fun onItemClick(v: View, position: Int) {
                val intent = Intent(
                    this@ListaDispositivosDsActivity,
                    FormularioDispositivoDsActivity::class.java
                ).apply {
                    putExtra(
                        "codAuto",
                        adapter.getDispositives()[position].codAuto
                    )
                }
                startActivity(intent)
            }
        })
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService),
                DatabaseHelperImpl(DatabaseBuilder.getInstance(applicationContext))
            )
        )[ListaDispositivosDsViewModel::class.java]
    }
}