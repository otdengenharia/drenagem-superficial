package com.otdengenharia.appotd.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.otdengenharia.appotd.R
import com.otdengenharia.appotd.api.ApiHelperImpl
import com.otdengenharia.appotd.api.RetrofitBuilder
import com.otdengenharia.appotd.databinding.ActivityLoginBinding
import com.otdengenharia.appotd.local.InfoViewModel
import com.otdengenharia.appotd.local.db.DatabaseBuilder
import com.otdengenharia.appotd.local.db.DatabaseHelperImpl
import com.otdengenharia.appotd.local.entity.Usuario
import com.otdengenharia.appotd.utils.ViewModelFactory

class LoginActivity : AppCompatActivity() {
    private lateinit var binding : ActivityLoginBinding
    private lateinit var viewModel: InfoViewModel
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupViewModel()
        efetuaLogin()
    }

    private fun saveSharedPreferences(usuario: Usuario) {
        sharedPreferences = getSharedPreferences("user_session",Context.MODE_PRIVATE) ?: return
        with(sharedPreferences.edit()) {
            putLong("id", usuario.id.toLong())
            putString("nome", usuario.nome)
            putString("email", usuario.email)
            putString("status", usuario.status)
            putString("nivel_acesso", usuario.nivelAcesso)
            commit()
        }
    }

    override fun onBackPressed() {
        return
    }

    private fun efetuaLogin() {
        binding.activityLoginBtn.setOnClickListener {
            verificaLogin()
        }
    }

    private fun verificaLogin() {
        var email: String = binding.activityLoginEmail.text.toString()
        var senha: String = binding.activityLoginSenha.text.toString()

//       email = "ti@otdengenharia.com"
//        senha = "otdti2022"


        if (email.isEmpty() || senha.isEmpty()) {
            binding.activityLoginMsgError.text = getString(R.string.msg_campos_obrigatorios_login)
        } else if (email.isNotEmpty() && senha.isNotEmpty()) {
            val usuarios: List<Usuario>? = viewModel.getUsuarios()
            if (usuarios != null) {
                val filtered = usuarios.filter {
                    it.email == email && it.senha == senha
                }
                if (filtered.isEmpty()){
                    binding.activityLoginMsgError.text = getString(R.string.msg_erro_login)
                } else {
                    saveSharedPreferences(filtered.first()).apply {
                        goToSelecaoActivity()
                    }
                }
            }else {
                binding.activityLoginMsgError.text = getString(R.string.msg_erro_cadastro_login)
            }
        }
    }

    private fun goToSelecaoActivity() {
        val intent = Intent(this@LoginActivity, SelecaoActivity::class.java)
        startActivity(intent)
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService),
                DatabaseHelperImpl(DatabaseBuilder.getInstance(applicationContext))
            )
        )[InfoViewModel::class.java]
    }
}