package com.otdengenharia.appotd.ui

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.lifecycle.ViewModelProviders
import com.otdengenharia.appotd.R
import com.otdengenharia.appotd.api.ApiHelperImpl
import com.otdengenharia.appotd.api.RetrofitBuilder
import com.otdengenharia.appotd.databinding.ActivityFotoDsBinding
import com.otdengenharia.appotd.local.FotoDsViewModel
import com.otdengenharia.appotd.local.db.DatabaseBuilder
import com.otdengenharia.appotd.local.db.DatabaseHelperImpl
import com.otdengenharia.appotd.local.entity.DrenagemSuperficial
import com.otdengenharia.appotd.local.entity.FotoDs
import com.otdengenharia.appotd.ui.recyclerview.adapter.ListaFotosAdapter
import com.otdengenharia.appotd.utils.Avaliado
import com.otdengenharia.appotd.utils.DateUtils
import com.otdengenharia.appotd.utils.ImageUtils
import com.otdengenharia.appotd.utils.ViewModelFactory
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class FotoDsActivity : AppCompatActivity() {
    private val REQUEST_CODE = 2
    private lateinit var binding: ActivityFotoDsBinding
    private lateinit var listaFotosAdapter: ListaFotosAdapter
    private lateinit var viewModel: FotoDsViewModel

    private var dispositivo: DrenagemSuperficial? = null
    private var codAuto: Long = 0L
    private var id: Long = 0L
    private var nomeFotoTmp: String? = null

    lateinit var currentPhotoPath: String
    private val REQUEST_TAKE_PHOTO = 1
    private val REQUEST_PICK_IMAGE = 2
    lateinit var photoURI: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFotoDsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        title = "Fotos Dispositivo"

        setupViewModel()
        setupToolbar()
        binding.activityFotoDsBtnCameraAdd.setOnClickListener {
            dispatchTakePictureIntent()
        }

        binding.activityFotoDsBtnGalleryAdd.setOnClickListener {
            openGallery()
        }
    }

    override fun onBackPressed() {
        return
    }

    private fun openGallery(){
        val intent = Intent("android.intent.action.GET_CONTENT")
        intent.type = "image/jpeg"
        intent.resolveActivity(packageManager)?.also {
            startActivityForResult(intent, REQUEST_PICK_IMAGE)
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Visualização Fotos"
    }

    override fun onResume() {
        super.onResume()

        if (intent.hasExtra("dispositiveDs")) {
            dispositivo = intent.getParcelableExtra("dispositiveDs")
            codAuto = dispositivo!!.codAuto
        }

        verificaFotoAntiga()

        val photosDs = viewModel.getPhotosDispositiveDs(codAuto)
        val numPhotos = photosDs?.size!!
        nomeFotoTmp = dispositivo?.identificacao + "_F" + numPhotos
        binding.activityFotoDsIdentificacaoDispositivo.text = dispositivo?.identificacao

        configurarListaFotosAdapter(photosDs)
    }

    private fun configurarListaFotosAdapter(arrayPhotos: List<FotoDs>) {
        if (arrayPhotos.isNotEmpty()) {
            listaFotosAdapter = ListaFotosAdapter(this, arrayPhotos as ArrayList<FotoDs>)
            binding.activityFotsDsListview.adapter = listaFotosAdapter
        }
    }

    private fun verificaFotoAntiga() {
        if (dispositivo?.foto1 == null && dispositivo?.foto2 == null) {
            binding.activityFotoDsFoto1.visibility = View.GONE
            binding.activityFotoDsFoto2.visibility = View.GONE
            binding.activityFotoDsDescricaoAntiga.visibility = View.GONE
        } else {
            if (!dispositivo?.foto1.isNullOrBlank()) {
                if (ImageUtils.readfile(dispositivo!!.foto1!!) != null) {
                    binding.activityFotoDsFoto1.setImageBitmap(
                        ImageUtils.readfile(
                            dispositivo!!.foto1!!
                        )
                    )
                }
            }

            if (!dispositivo?.foto2.isNullOrBlank()) {
                if (ImageUtils.readfile(dispositivo!!.foto2!!) != null) {
                    binding.activityFotoDsFoto2.setImageBitmap(
                        ImageUtils.readfile(
                            dispositivo!!.foto2!!
                        )
                    )
                }
            }
        }
    }

    private fun atualizaStatusDispositivo() {
        dispositivo?.let {
            it.data = DateUtils.getFormattedDate()
            if (it.avaliado == Avaliado.PENDENTE.toString()) {
                it.avaliado = Avaliado.ABERTO.toString()
            } else {
                if (it.ok!!.isNotEmpty()){
                    it.avaliado = Avaliado.OK.toString()
                }
            }
            viewModel.updateDispositiveDs(it)
        }
    }


    @Throws(IOException::class)
    private fun createImageFile(): File {
        var dir = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS+"/fotos_camera_ds")
                .toURI()
        )

        if (!dir.exists()){
            dir.mkdirs()
        }

        // Create an image file name
        return File.createTempFile(
            nomeFotoTmp, /* prefix */
            ".jpg", /* suffix */
            dir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    photoURI = FileProvider.getUriForFile(
                        this,
                        "com.otdengenharia.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            //val imageUri = data?.getData() as Uri
            val imageUri = photoURI
            Log.i("photoURI", photoURI.toString())
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                ImageDecoder.decodeBitmap(
                    ImageDecoder.createSource(
                        contentResolver,
                        imageUri
                    )
                )
            } else {
                MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
            }
            salvarFotoTabela()
            atualizaStatusDispositivo()
            Toast.makeText(this, "Foto adiconada com sucesso!", Toast.LENGTH_LONG).show()
        }
        else if (requestCode == REQUEST_PICK_IMAGE) {
            val p = ImageUtils.uriToBitmap(data?.data!!, this)

            val dir = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS+"/fotos_galeria_ds")
                    .toURI()
            )

            if (!dir.exists()){
                dir.mkdirs()
            }

            photoURI = FileProvider.getUriForFile(
                this,
                "com.otdengenharia.fileprovider",
                dir
            )

            val file = File(dir, "$nomeFotoTmp.jpg")
            file.createNewFile()

            try {
                val out = FileOutputStream(file)
                p.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            currentPhotoPath = "$photoURI/$nomeFotoTmp.jpg"
            photoURI = currentPhotoPath.toUri()
            nomeFotoTmp = "$nomeFotoTmp.jpg"
            salvarFotoTabela()
            atualizaStatusDispositivo()
            Toast.makeText(this, "Foto adiconada com sucesso!", Toast.LENGTH_LONG).show()
        }
    }

    private fun getNomeFotoCompleto(): String {
        val path = currentPhotoPath.split("/")
        return path[path.size - 1]
    }

    private fun salvarFotoTabela() {
        val nome = if (nomeFotoTmp!!.contains(".jpg"))  nomeFotoTmp else "${nomeFotoTmp}.jpg"
        val foto = FotoDs(
            id,
            dispositivo!!.codAuto,
            nome!!,
            photoURI.toString(),
            observacao = "",
            data = DateUtils.getFormattedDate()
        )
        viewModel.insertPhotoDispositiveDs(foto)
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService),
                DatabaseHelperImpl(DatabaseBuilder.getInstance(applicationContext))
            )
        )[FotoDsViewModel::class.java]
    }
}