package com.otdengenharia.appotd.ui.recyclerview.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.otdengenharia.appotd.R
import com.otdengenharia.appotd.local.entity.DrenagemSuperficial
import com.otdengenharia.appotd.local.entity.FotoDs
import com.otdengenharia.appotd.utils.ImageUtils
import java.util.*

class RecyclerAdapterDrenagemSuperficial(
    private val context: Context,
    private var dispositivos: MutableList<DrenagemSuperficial> = mutableListOf(),
    private var fotos: MutableList<FotoDs> = mutableListOf()
) : RecyclerView.Adapter<RecyclerAdapterDrenagemSuperficial.ViewHolder>() {
    private lateinit var listener: OnItemClickListener
    private var copyList: MutableList<DrenagemSuperficial> = dispositivos

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerAdapterDrenagemSuperficial.ViewHolder {
        val viewCriada = LayoutInflater.from(context)
            .inflate(R.layout.card_layout_dispositivos_ds, parent, false)
        return ViewHolder(viewCriada)
    }

    override fun onBindViewHolder(
        holder: RecyclerAdapterDrenagemSuperficial.ViewHolder,
        position: Int
    ) {
        val dispositivo = dispositivos[position]
        holder.bind(dispositivo)
    }

    override fun getItemCount(): Int {
        return dispositivos.size
    }


    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    fun add(dispositivos: List<DrenagemSuperficial>) {
        this.dispositivos.addAll(dispositivos)
        notifyItemRangeChanged(0, dispositivos.size)
    }

    fun getDispositives(): MutableList<DrenagemSuperficial> {
        return this.dispositivos
    }

    fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val charSequenceString = constraint.toString()
                dispositivos = if (charSequenceString.isNotEmpty()) {
                    val filteredAuxList: MutableList<DrenagemSuperficial> = ArrayList()
                    copyList.forEach {
                        if (it.identificacao.lowercase(Locale.getDefault())
                                .contains(charSequenceString.lowercase(Locale.getDefault()))
                        ) {
                            filteredAuxList.add(it)
                        }
                    }
                    filteredAuxList
                } else {
                    copyList
                }
                val results = FilterResults()
                results.values = dispositivos
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults) {
                dispositivos = results.values as MutableList<DrenagemSuperficial>
                notifyDataSetChanged()
            }
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        private val identificacao =
            itemView.findViewById<TextView>(R.id.card_layout_dispositivos_ds_id)
        private val elemento =
            itemView.findViewById<TextView>(R.id.card_layout_dispositivos_ds_element)
        private val km = itemView.findViewById<TextView>(R.id.card_layout_dispositivos_ds_km)
        private val sentido =
            itemView.findViewById<TextView>(R.id.card_layout_dispositivos_ds_sentido)
        private val segmento =
            itemView.findViewById<TextView>(R.id.card_layout_dispositivos_ds_segmento)
        private val foto1 = itemView.findViewById<ImageView>(R.id.card_layout_dispositivos_ds_photo)
        private val foto2 =
            itemView.findViewById<ImageView>(R.id.card_layout_dispositivos_ds_photo2)
        private val status =
            itemView.findViewById<TextView>(R.id.card_layout_dispositivos_ds_status)

        fun bind(dispositivo: DrenagemSuperficial) {
            identificacao.text = dispositivo.identificacao
            elemento.text = dispositivo.elemento
            km.text = dispositivo.km
            sentido.text = dispositivo.sentido
            segmento.text = dispositivo.segmento.toString()
            status.text = dispositivo.avaliado

            if (dispositivo.avaliado == "OK") {
                status.setTextColor(Color.rgb(0, 153, 0))
            } else if (dispositivo.avaliado == "ABERTO") {
                status.setTextColor(Color.rgb(255, 128, 0))
            } else {
                status.setTextColor(Color.RED)
            }

            val fotos = fotos.filter {
                it.ficha == dispositivo.codAuto
            }

            if (dispositivo.foto1 == null) run {
                if (fotos.isNotEmpty()) {
                    fotos[0].let {
                        val bitmap = ImageUtils.uriToBitmap(it.path!!.toUri(), context)
                        bitmap.apply {
                            foto1.setImageBitmap(this)
                        }
                    }
                }
            }

            if (dispositivo.foto2 == null) run {
                if (fotos.isNotEmpty() && fotos.size > 1) {
                    fotos[1].let {
                        val bitmap = ImageUtils.uriToBitmap(it.path!!.toUri(), context)
                        bitmap.apply {
                            foto2.setImageBitmap(this)
                        }
                    }
                }
            }

            if (dispositivo.foto1 != null && dispositivo.foto2 != null) {
                val filename1 = dispositivo.foto1!!
                val filename2 = dispositivo.foto2!!

                val bitmap1 = ImageUtils.readfile(filename1)
                bitmap1.apply {
                    foto1.setImageBitmap(this)
                }

                val bitmap2 = ImageUtils.readfile(filename2)
                bitmap2.apply {
                    foto2.setImageBitmap(this)
                }
            }
        }

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (v != null) {
                listener.onItemClick(v, adapterPosition)
            }
        }

    }

    interface OnItemClickListener {
        fun onItemClick(v: View, position: Int)
    }

}