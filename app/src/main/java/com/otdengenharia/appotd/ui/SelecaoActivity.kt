package com.otdengenharia.appotd.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.navigation.NavigationView
import com.otdengenharia.appotd.R
import com.otdengenharia.appotd.api.ApiHelperImpl
import com.otdengenharia.appotd.api.RetrofitBuilder
import com.otdengenharia.appotd.databinding.ActivitySelecaoBinding
import com.otdengenharia.appotd.local.InfoViewModel
import com.otdengenharia.appotd.local.db.DatabaseBuilder
import com.otdengenharia.appotd.local.db.DatabaseHelperImpl
import com.otdengenharia.appotd.local.entity.Info
import com.otdengenharia.appotd.local.entity.Usuario
import com.otdengenharia.appotd.utils.Status
import com.otdengenharia.appotd.utils.ViewModelFactory
import kotlin.properties.Delegates

class SelecaoActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var binding: ActivitySelecaoBinding
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navView: NavigationView
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var viewModel: InfoViewModel
    private var idInfoCampanha by Delegates.notNull<Int>()
    private val DISCIPLINA = "DS"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelecaoBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val userSession = getUserSession()
        val nome = userSession?.nome
        val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Drenagem Superficial"
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        val headerView = navView.getHeaderView(0)
        headerView.findViewById<TextView>(R.id.nav_header_colaborador).text = nome

        setupViewModel()
        val id = userSession?.id
        setupObserver(id?.toLong()!!, DISCIPLINA)

        val btnSelecionar = findViewById<Button>(R.id.content_main_btn)
        btnSelecionar.setOnClickListener {
            goToListaDispositivosActivity()
        }

    }

    override fun onBackPressed() {
        return
    }

    private fun goToListaDispositivosActivity() {
        val intent = Intent(this@SelecaoActivity, ListaDispositivosDsActivity::class.java)
        intent.putExtra("idInfoCampanha", idInfoCampanha)
        startActivity(intent)
    }

    private fun getUserSession(): Usuario? {
        sharedPreferences = getSharedPreferences("user_session", MODE_PRIVATE)
        return if (sharedPreferences.all.isNotEmpty()){
            val userId = sharedPreferences.getLong("id", 0L)
            val nome = sharedPreferences.getString("nome", "")
            val email = sharedPreferences.getString("email", "")
            val status = sharedPreferences.getString("status", "")
            val nivelAcesso = sharedPreferences.getString("nivel_acesso", "")
            Usuario(userId.toInt(), nome!!, email!!, "", nivelAcesso!!,status!!)
        } else {
            null
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_menu_info_campanha -> {

            }
            R.id.nav_menu_selecionar_campanha -> {

            }
            R.id.nav_menu_upload -> {

            }
            R.id.nav_menu_download -> {

            }
            R.id.nav_menu_logout -> {
                endUserSession()
                endInfoCampanhaSession()
                goToLoginActivity()
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun endInfoCampanhaSession() {
        sharedPreferences = getSharedPreferences("info_campanha_session", MODE_PRIVATE)
        sharedPreferences.all.clear()
    }

    private fun endUserSession() {
        sharedPreferences = getSharedPreferences("user_session", MODE_PRIVATE)
        sharedPreferences.all.clear()
    }

    private fun goToLoginActivity() {
        val intent = Intent(this@SelecaoActivity, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun setupObserver(id: Long, disciplina: String) {
        val progressBar = findViewById<ProgressBar>(R.id.content_main_progressbar)
        val msgLoading = findViewById<TextView>(R.id.content_main_msg)
        val msgHeader = findViewById<TextView>(R.id.content_main_msg_header)
        val campanhas = findViewById<AutoCompleteTextView>(R.id.content_main_text_spinner)
        val layoutSpinnerCampanha =
            findViewById<com.google.android.material.textfield.TextInputLayout>(R.id.content_main_layout_spinner)
        val btnSelect = findViewById<Button>(R.id.content_main_btn)

        viewModel.getInfoCampanhas(id, disciplina).observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    msgLoading.text = getString(R.string.msg_ataulizacao_ok)
                    msgLoading.visibility = View.GONE
                    msgHeader.visibility = View.VISIBLE
                    campanhas.visibility = View.VISIBLE
                    btnSelect.visibility = View.VISIBLE
                    layoutSpinnerCampanha.visibility = View.VISIBLE

                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    msgLoading.text = getString(R.string.msg_atualizacao_loading)
                    msgLoading.visibility = View.GONE
                    msgHeader.visibility = View.GONE
                    campanhas.visibility = View.GONE
                    btnSelect.visibility = View.GONE
                    layoutSpinnerCampanha.visibility = View.GONE
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    msgLoading.visibility = View.VISIBLE
                    msgLoading.text = it.message
                }
            }
            configuraSpinnerCampanha()
        }
    }

    private fun configuraSpinnerCampanha() {
        val info = mutableListOf<String>()
        val msgHeader = findViewById<TextView>(R.id.content_main_msg_header)
        val campanhas = findViewById<AutoCompleteTextView>(R.id.content_main_text_spinner)
        val msgLoading = findViewById<TextView>(R.id.content_main_msg)
        val layoutSpinnerCampanha =
            findViewById<com.google.android.material.textfield.TextInputLayout>(R.id.content_main_layout_spinner)
        val btnSelect = findViewById<Button>(R.id.content_main_btn)

        var data : List<Info>  = viewModel.findInfoByUsuarioId(getUserSession()?.id!!.toLong())

        if (data.isNotEmpty()){
            msgHeader.visibility = View.VISIBLE
            campanhas.visibility = View.VISIBLE
            btnSelect.visibility = View.VISIBLE
            layoutSpinnerCampanha.visibility = View.VISIBLE
            msgLoading.visibility = View.GONE
        } else {
            msgLoading.text = "Nenhuma campanha disponível."
        }

        data.forEach {
            info.add("${it.id}. ${it.nomeConcessionaria} (${it.dataInicio})")
        }

        val arrayAdapter =
            ArrayAdapter(this@SelecaoActivity, R.layout.dropdown_item, info)
        campanhas.setAdapter(arrayAdapter)

        var selectedItem: String
        campanhas.setOnItemClickListener { adapterView, _, i, _ ->
            selectedItem = adapterView.getItemAtPosition(i).toString()
            val position = selectedItem.split(".").first().toInt() - 1
            setInfoCampanhaSelecionada(data, position)

            findViewById<Button>(R.id.content_main_btn).isEnabled = true

            idInfoCampanha = data[position].id!!
            val campanha = viewModel.getInfoCampanha(idInfoCampanha.toLong())
            saveSharedPreferences(campanha)
            return@setOnItemClickListener
        }
    }

    private fun saveSharedPreferences(campanha: Info) {
        sharedPreferences = getSharedPreferences("info_campanha_session", MODE_PRIVATE)
            ?: return
        with(sharedPreferences.edit()) {
            putInt("id_campanha", campanha.id!!)
            putInt("usuario_id", campanha.usuarioId)
            putInt("campanha_id", campanha.campanhaId)
            putInt("concessionaria_id", campanha.concessionariaId)
            putString("nome_concessionaria", campanha.nomeConcessionaria)
            putString("disciplina", campanha.disciplina)
            putString("data_inicio", campanha.dataInicio)
            putString("data_final", campanha.dataFinal)
            putString("logo", campanha.logo)
            commit()
        }
    }

    private fun setInfoCampanhaSelecionada(
        data: List<Info>?,
        position: Int
    ) {
        if (data != null) {
            setLogoConcessionaria(data, position)
            setNomeConcessioaria(data, position)
            setDataInicioCampanha(data, position)
        }
    }

    private fun setDataInicioCampanha(
        data: List<Info>,
        position: Int
    ) {
        val dataInicio = findViewById<TextView>(R.id.content_main_data_inicio)
        dataInicio.text = "Data Início: ${data[position].dataInicio}"
    }

    @SuppressLint("SetTextI18n")
    private fun setNomeConcessioaria(
        data: List<Info>,
        position: Int
    ) {
        val nomeConcessionaria = findViewById<TextView>(R.id.content_main_concessionaria)
        nomeConcessionaria.text =
            "Nome Concessionária: ${data[position].nomeConcessionaria}"
    }

    private fun setLogoConcessionaria(
        data: List<Info>,
        position: Int
    ) {
        val logo = findViewById<ImageView>(R.id.content_main_logo)
        val imageName = data[position].logo.split(".png").first()
        val imageResource = resources.getIdentifier(imageName, "drawable", packageName)
        val draw = AppCompatResources.getDrawable(this, imageResource)
        logo.setImageDrawable(draw)
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService),
                DatabaseHelperImpl(DatabaseBuilder.getInstance(applicationContext))
            )
        )[InfoViewModel::class.java]
    }
}