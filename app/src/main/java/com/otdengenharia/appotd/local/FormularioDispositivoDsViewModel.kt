package com.otdengenharia.appotd.local

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.otdengenharia.appotd.local.db.DatabaseHelper
import com.otdengenharia.appotd.local.entity.DrenagemSuperficial
import kotlinx.coroutines.launch

class FormularioDispositivoDsViewModel(
    private val dbHelper: DatabaseHelper
)  : ViewModel()  {

    fun insertDispositiveDs(dispositive: DrenagemSuperficial) {
        viewModelScope.launch {
            dbHelper.insertDispositivesDs(dispositive)
        }
    }

    fun verifyIdentification(identification : String) : Int {
        var numIdentification = 0
        viewModelScope.launch {
            numIdentification = dbHelper.verifyId(identification)
        }
        return numIdentification
    }

    fun getMaxCodAuto() : Long {
        var maxCodAuto = 0
        viewModelScope.launch {
            maxCodAuto = dbHelper.getMaxCodAuto().toInt()
        }
        return maxCodAuto.toLong()
    }

    fun getDispositiveDs(codAuto : Long) : DrenagemSuperficial {
        var disp : DrenagemSuperficial? = null
        viewModelScope.launch {
            disp = dbHelper.getDispositiveDs(codAuto)
        }
        return disp!!
    }

    fun updateDispositiveDs(disp: DrenagemSuperficial) {
        viewModelScope.launch {
            dbHelper.updateDispositiveDs(disp)
        }
    }
}