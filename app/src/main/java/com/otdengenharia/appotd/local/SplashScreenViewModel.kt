package com.otdengenharia.appotd.local

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.otdengenharia.appotd.api.ApiHelper
import com.otdengenharia.appotd.local.db.DatabaseHelper
import com.otdengenharia.appotd.local.entity.Usuario
import com.otdengenharia.appotd.utils.Resource
import kotlinx.coroutines.launch

class SplashScreenViewModel (
    private val apiHelper: ApiHelper,
    private val dbHelper: DatabaseHelper
        ) : ViewModel() {
            private val userResource = MutableLiveData<Resource<List<Usuario>>>()
    init {
        fetchInfoUsuarios()
    }

    private fun fetchInfoUsuarios(){
        viewModelScope.launch {
            userResource.postValue(Resource.loading(null))

            try {
                val infoUsuariosApi = apiHelper.getInfoUsuarios()
                val infoUsuariosDB = mutableListOf<Usuario>()

                for (obj in infoUsuariosApi){
                    val infoUser = obj.toEntity()
                    infoUsuariosDB.add(infoUser)
                }
                dbHelper.insertUsuarios(infoUsuariosDB)
                userResource.postValue(Resource.success(infoUsuariosDB))
            } catch (e: Exception) {
                userResource.postValue(Resource.error("Lista de usuários não atualizada.", null))
            }
        }
    }

    fun getInfoUsuarios(): LiveData<Resource<List<Usuario>>> {
        return userResource
    }
}