package com.otdengenharia.appotd.local.db

import com.otdengenharia.appotd.local.entity.DrenagemSuperficial
import com.otdengenharia.appotd.local.entity.FotoDs
import com.otdengenharia.appotd.local.entity.Info
import com.otdengenharia.appotd.local.entity.Usuario

interface DatabaseHelper {
    suspend fun getDispositivesDs() : List<DrenagemSuperficial>

    suspend fun getDispositiveDs(codAuto : Long) : DrenagemSuperficial

    suspend fun insertAllDispositivesDs(dispositives : List<DrenagemSuperficial>)

    suspend fun insertDispositivesDs(vararg dispositive : DrenagemSuperficial)

    suspend fun updateDispositiveDs(dispositive : DrenagemSuperficial)

    fun countDispositivesDs(state : String) : Int

    suspend fun findByDate(date: String): List<DrenagemSuperficial>

    fun countDispositivesDsByDate(state : String, date : String) : Int

    suspend fun verifyId(id: String): Int

    suspend fun getMaxCodAuto() : Long

    suspend fun getPhotosDispositivesDs() : List<FotoDs>

    suspend fun insertPhotosDispositivesDs(photos : FotoDs)

    suspend fun getPhotosDispositivesDsByCodAuto(codAuto: Long): List<FotoDs>

    suspend fun findBySegmento(segmento: Int): List<DrenagemSuperficial>

    suspend fun insertUsuarios(usuarios : List<Usuario>)

    suspend fun findAllUsuarios() : List<Usuario>

    suspend fun insertInfo(info : List<Info>)

    suspend fun findById(id: Long): Info

    suspend fun findAllInfo() : List<Info>

    suspend fun findByUsuarioId(id: Long): List<Info>
}