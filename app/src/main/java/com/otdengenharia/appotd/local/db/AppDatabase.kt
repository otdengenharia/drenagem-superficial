package com.otdengenharia.appotd.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.otdengenharia.appotd.local.dao.DrenagemSuperficialDao
import com.otdengenharia.appotd.local.dao.FotoDsDao
import com.otdengenharia.appotd.local.dao.InfoDao
import com.otdengenharia.appotd.local.dao.UsuarioDao
import com.otdengenharia.appotd.local.entity.DrenagemSuperficial
import com.otdengenharia.appotd.local.entity.FotoDs
import com.otdengenharia.appotd.local.entity.Info
import com.otdengenharia.appotd.local.entity.Usuario

@Database(entities = [DrenagemSuperficial::class, FotoDs::class, Usuario::class, Info::class], version = 5)
abstract class AppDatabase : RoomDatabase(){
    abstract fun drenagemSuperficialDao() : DrenagemSuperficialDao
    abstract fun fotoDsDao() : FotoDsDao
    abstract fun usuarioDao() : UsuarioDao
    abstract fun infoDao() : InfoDao
}