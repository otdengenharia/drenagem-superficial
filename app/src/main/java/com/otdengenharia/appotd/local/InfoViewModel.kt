package com.otdengenharia.appotd.local

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.otdengenharia.appotd.api.ApiHelper
import com.otdengenharia.appotd.local.db.DatabaseHelper
import com.otdengenharia.appotd.local.entity.Info
import com.otdengenharia.appotd.local.entity.Usuario
import com.otdengenharia.appotd.utils.Resource
import kotlinx.coroutines.launch

data class InfoViewModel(
    private val apiHelper: ApiHelper,
    private val dbHelper: DatabaseHelper
) : ViewModel() {
    private val infoResource = MutableLiveData<Resource<List<Info>>>()

    private fun fetchInfoCampanhas(id: Long, disciplina: String) {
        viewModelScope.launch {
            infoResource.postValue(Resource.loading(null))

            try {
                val infoCampanhasApi = apiHelper.getInfoCampanhas(id, disciplina)
                val infoCampanhasDB = mutableListOf<Info>()
                for (obj in infoCampanhasApi) {
                    val infoUser = obj.toEntity()
                    infoCampanhasDB.add(infoUser)
                }
                dbHelper.insertInfo(infoCampanhasDB)
                infoResource.postValue(Resource.success(infoCampanhasDB))
            } catch (e: Exception) {
                infoResource.postValue(
                    Resource.error(
                        "Informações das campanhas não atualizadas.",
                        null
                    )
                )
            }
        }
    }

    fun getInfoCampanhas(id: Long, disciplina: String): LiveData<Resource<List<Info>>> {
        fetchInfoCampanhas(id, disciplina)
        return infoResource
    }

    fun getUsuarios(): List<Usuario>? {
        var usuarios: List<Usuario>? = null

        viewModelScope.launch {
            usuarios = dbHelper.findAllUsuarios()
        }
        return usuarios!!
    }

    fun getInfoCampanha(id: Long): Info {
        var info: Info? = null
        viewModelScope.launch {
            info = dbHelper.findById(id)
        }
        return info!!
    }

    fun findInfoByUsuarioId(id: Long) : List<Info> {
        lateinit var findAllInfo: List<Info>
        viewModelScope.launch {
            findAllInfo = dbHelper.findByUsuarioId(id)
        }
        return findAllInfo
    }
}