package com.otdengenharia.appotd.local

import android.content.ContentValues
import android.content.Context
import android.os.Build
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.util.ArrayMap
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.GsonBuilder
import com.otdengenharia.appotd.api.ApiHelper
import com.otdengenharia.appotd.local.db.DatabaseHelper
import com.otdengenharia.appotd.local.entity.DrenagemSuperficial
import com.otdengenharia.appotd.local.entity.FotoDs
import com.otdengenharia.appotd.model.ApiDrenagemSuperficial
import com.otdengenharia.appotd.utils.Avaliado
import com.otdengenharia.appotd.utils.DateUtils
import com.otdengenharia.appotd.utils.ImageUtils
import com.otdengenharia.appotd.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.RequestBody
import java.net.URL
import kotlin.properties.Delegates


class ListaDispositivosDsViewModel(
    private val apiHelper: ApiHelper,
    private val dbHelper: DatabaseHelper
) :
    ViewModel() {

    private val dispositivesDs = MutableLiveData<Resource<List<DrenagemSuperficial>>>()
    private val responsePost = MutableLiveData<Resource<List<DrenagemSuperficial>>>()
    private val BASEURL: String = "https://otdengenharia.com/apiotd1409/fotosAntigas/ds/"
    private val photosDs = MutableLiveData<Resource<List<String>>>()
    private var segmento by Delegates.notNull<Int>()

    private fun fetchDispositivesDs(campanhaId : Long) {
        viewModelScope.launch {
            dispositivesDs.postValue(Resource.loading(null))
            try {
                val dispositivesDsFromDb = dbHelper.getDispositivesDs()

                if (dispositivesDsFromDb.isEmpty()) {
                    val dispositivesDsFromApi = apiHelper.getDispositivesDs(campanhaId)
                    val dispositivesDsToInsertInDB = mutableListOf<DrenagemSuperficial>()

                    for (apiDispositiveDs in dispositivesDsFromApi) {
                        val dispositiveDs = apiDispositiveDs.toEntity()
                        dispositivesDsToInsertInDB.add(dispositiveDs)
                    }
                    dbHelper.insertAllDispositivesDs(dispositivesDsToInsertInDB)
                    dispositivesDs.postValue(Resource.success(dispositivesDsToInsertInDB))
                } else {
                    dispositivesDs.postValue(Resource.success(dispositivesDsFromDb))
                   /* if (dispositivesDsFromDb.first().campanhaId==campanhaId) {
                        dispositivesDs.postValue(Resource.success(dispositivesDsFromDb))
                    } else {
                        //upload fichas
                        //upload fotos
                        //delete tabela local DrenagemSuperficial
                        //Tenta Conectar com API
                    }*/
                }
            } catch (e: Exception) {
                dispositivesDs.postValue(Resource.error("Não foi possível estabelecer conxeão com o servidor", null))
            }
        }
    }

    fun getDispositivesDs(campanhaId : Long): LiveData<Resource<List<DrenagemSuperficial>>> {
        fetchDispositivesDs(campanhaId)
        return dispositivesDs
    }

    fun statistics(): ArrayMap<String, Int> {
        val statistics: ArrayMap<String, Int> = ArrayMap()

        statistics[Avaliado.OK.toString()] = dbHelper.countDispositivesDs(Avaliado.OK.toString())
        statistics["OK_DAY"] = dbHelper.countDispositivesDsByDate(
            Avaliado.OK.toString(),
            DateUtils.getFormattedDate()
        )
        statistics[Avaliado.ABERTO.toString()] =
            dbHelper.countDispositivesDs(Avaliado.ABERTO.toString())
        statistics[Avaliado.PENDENTE.toString()] =
            dbHelper.countDispositivesDs(Avaliado.PENDENTE.toString())

        return statistics
    }

    fun insertDispositiveDs(dispositive: DrenagemSuperficial) {
        viewModelScope.launch {
            dbHelper.insertDispositivesDs(dispositive)
        }
    }

    fun getDispositivesDsByDate(date: String): List<DrenagemSuperficial> {
        var list: List<DrenagemSuperficial>? = null
        viewModelScope.launch {
            list = dbHelper.findByDate(date)
        }
        return list!!
    }

    fun sendDispositivesDs() {

        viewModelScope.launch {

            val findByDate = dbHelper.findByDate(DateUtils.getFormattedDate())
            Log.i("findByDate", findByDate.toString())
            val dispositivesDsApi = mutableListOf<ApiDrenagemSuperficial>()
            responsePost.postValue(Resource.loading(null))
            findByDate.forEach {
                val dispositive = it.toModel()
                if (dispositive.sincronizado == "N") {
                    dispositivesDsApi.add(dispositive)
                }
            }

            val gson = GsonBuilder().create()
            val myCustomArray = gson.toJsonTree(dispositivesDsApi).asJsonArray

            Log.i("gsonArray", myCustomArray.toString())

            val requestBody =
                RequestBody.create(
                    MediaType.parse("application/json; charset=utf-8"), myCustomArray.toString()
                )


            Log.i("requestBody", requestBody.toString())
            val sendDispositiveDs = apiHelper.sendDispositiveDs(requestBody)
            Log.i("Response", sendDispositiveDs.toString())

            if (sendDispositiveDs.code() == 200) {
                dispositivesDsApi.forEach {
                    it.sincronizado = "S"
                    dbHelper.insertDispositivesDs(it.toEntity())
                }
                responsePost.postValue(Resource.success(null))
            } else {
                responsePost.postValue(Resource.error("Erro: Upload não concluído", null))
            }
        }
    }

    private fun uploadImages(context: Context, campanhaId: Long) {
        viewModelScope.launch {
//            var photos = dbHelper.getPhotosDispositivesDs().filter { it.sincronizado != "S" }
            val photos = dbHelper.getPhotosDispositivesDs()
            Log.i("photos", photos.toString())

            photos.forEach {
                photosDs.postValue(Resource.loading(null))
                try {
                    val bitmap = ImageUtils.uriToBitmap(it.path!!.toUri(), context)
                    val image = ImageUtils.convertToString(bitmap)
                    val response = apiHelper.uploadImage(it.nome, image!!, it.ficha,campanhaId)
                } catch (e : Exception){
                    photosDs.postValue(Resource.error("Erro: Upload não concluído", null))
                    Toast.makeText(context, "Falha no envio da imagem", Toast.LENGTH_SHORT).show()
                }
            }
            photosDs.postValue(Resource.success(null))
        }
    }

    fun uploadImagesAndDispositives(context: Context, campanhaId: Long) {
        sendDispositivesDs()
        uploadImages(context, campanhaId)
    }

    fun getAllPhotos(): List<FotoDs> {
        var listPhotos: List<FotoDs>? = null
        viewModelScope.launch {
            listPhotos = dbHelper.getPhotosDispositivesDs()
        }
        return listPhotos!!
    }

    fun getResponseDispositivesDs(): LiveData<Resource<List<DrenagemSuperficial>>> {
        return responsePost
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private suspend fun fetchPhotosDispositivesDs(context: Context, nomeCampanha : String) {
        viewModelScope.launch {

            try {
                dbHelper.findBySegmento(segmento).forEach { imageBase64 ->
                    photosDs.postValue(Resource.loading(null))
                    val photo1Renamed =
                        imageBase64.foto1?.replace(" ", "%20")?.replace("+","%2B")
                    val photo2Renamed =
                        imageBase64.foto2?.replace(" ", "%20")?.replace("+","%2B")

                    imageBase64.foto1.toString()
                        .let {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                if (!imageBase64.foto1.isNullOrBlank()) {
                                    downloadFile(context, "$BASEURL$nomeCampanha/$photo1Renamed", it)
                                }
                            }
                        }
                    imageBase64.foto2.toString()
                        .let {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                if (!imageBase64.foto2.isNullOrBlank()) {
                                    downloadFile(context, "$BASEURL$nomeCampanha/$photo2Renamed", it)
                                }
                            }
                        }
                }
                photosDs.postValue(Resource.success(null))
            } catch (e: Exception) {
                photosDs.postValue(Resource.error("Houve algum problema no download das fotos.", null))
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private suspend fun downloadFile(context: Context, url: String, fileName: String) {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        withContext(Dispatchers.IO) {
            val contentValues = ContentValues().apply {
                put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
                put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS)
            }
            val resolver = context.contentResolver
            val uri = resolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues)
            if (uri != null) {
                URL(url).openStream().use { input ->
                    resolver.openOutputStream(uri).use { output ->
                        input.copyTo(output!!, DEFAULT_BUFFER_SIZE)
                    }
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @JvmName("setSegmento1")
    fun setSegmento(context: Context, segmento: Int, nomeCampanha: String) {
        this.segmento = segmento
        viewModelScope.launch {
            fetchPhotosDispositivesDs(context, nomeCampanha)
        }
    }

    fun getPhotoDispositivesDs(): LiveData<Resource<List<String>>> {
        return photosDs
    }

    fun countDispositivesBySegmento(segmento: String): Int {
        var count = 0
        viewModelScope.launch {
            count = dbHelper.findBySegmento(segmento.toInt()).size
        }
        return count * 2
    }

    fun counPhotosDispositivesDs() : Int {
        var count = 0
        viewModelScope.launch {
            count = dbHelper.getPhotosDispositivesDs().size
        }
        return count
    }

}