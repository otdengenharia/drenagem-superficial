package com.otdengenharia.appotd.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.otdengenharia.appotd.local.entity.Usuario

@Dao
interface UsuarioDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAll( usuarios: List<Usuario>)

    @Query("SELECT * FROM Usuario")
    fun findAll(): List<Usuario>
}