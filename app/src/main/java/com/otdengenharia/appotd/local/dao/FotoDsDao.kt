package com.otdengenharia.appotd.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.otdengenharia.appotd.local.entity.FotoDs

@Dao
interface FotoDsDao {
    @Query("SELECT * FROM FotoDs")
    fun all(): List<FotoDs>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(fotoDs: FotoDs)

    @Query("SELECT * FROM FotoDs WHERE ficha = :codAuto")
    fun findByCodAuto(codAuto: Long): List<FotoDs>

    @Query("SELECT * FROM FotoDs WHERE id = :id")
    fun findById(id: Long): FotoDs

    @Query("SELECT * FROM FotoDs WHERE sincronizado = 'N'")
    fun findNotSync(): List<FotoDs>

    @Query("SELECT COUNT(*) FROM FotoDs WHERE ficha = :ficha")
    fun countByFicha(ficha: Long): Int

    @Query("SELECT MAX(id) FROM FotoDs")
    fun getMaxCodAuto(): Long

}