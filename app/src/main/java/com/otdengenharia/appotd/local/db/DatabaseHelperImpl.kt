package com.otdengenharia.appotd.local.db

import com.otdengenharia.appotd.local.entity.DrenagemSuperficial
import com.otdengenharia.appotd.local.entity.FotoDs
import com.otdengenharia.appotd.local.entity.Info
import com.otdengenharia.appotd.local.entity.Usuario

class DatabaseHelperImpl(private val appDatabase: AppDatabase) : DatabaseHelper {
    override suspend fun getDispositivesDs(): List<DrenagemSuperficial> {
        return appDatabase.drenagemSuperficialDao().all()
    }

    override suspend fun getDispositiveDs(codAuto : Long): DrenagemSuperficial {
        return appDatabase.drenagemSuperficialDao().findByCodAuto(codAuto)
    }

    override suspend fun insertAllDispositivesDs(dispositives: List<DrenagemSuperficial>) {
        return appDatabase.drenagemSuperficialDao().addAll(dispositives)
    }


    override suspend fun insertDispositivesDs(vararg dispositive: DrenagemSuperficial) {
        return appDatabase.drenagemSuperficialDao().add(*dispositive)
    }

    override suspend fun updateDispositiveDs(dispositive: DrenagemSuperficial) {
        return appDatabase.drenagemSuperficialDao().update(dispositive)
    }

    override fun countDispositivesDs(state: String): Int {
        return appDatabase.drenagemSuperficialDao().count(state)
    }

    override suspend fun findByDate(date: String): List<DrenagemSuperficial> {
        return appDatabase.drenagemSuperficialDao().findByDate(date)
    }

    override fun countDispositivesDsByDate(state: String, date: String): Int {
        return appDatabase.drenagemSuperficialDao().countByDate(state,date)
    }

    override suspend fun verifyId(id: String): Int {
        return appDatabase.drenagemSuperficialDao().verifyId(id)
    }

    override suspend fun getMaxCodAuto(): Long {
        return appDatabase.drenagemSuperficialDao().getMaxCodAuto()
    }

    override suspend fun getPhotosDispositivesDs(): List<FotoDs> {
        return appDatabase.fotoDsDao().all()
    }

    override suspend fun insertPhotosDispositivesDs(photos: FotoDs) {
        return appDatabase.fotoDsDao().add(photos)
    }

    override suspend fun getPhotosDispositivesDsByCodAuto(codAuto: Long): List<FotoDs> {
        return appDatabase.fotoDsDao().findByCodAuto(codAuto)
    }

    override suspend fun findBySegmento(segmento: Int): List<DrenagemSuperficial> {
        return  appDatabase.drenagemSuperficialDao().findBySegmento(segmento)
    }

    override suspend fun insertUsuarios(usuarios: List<Usuario>) {
        return appDatabase.usuarioDao().addAll(usuarios)
    }

    override suspend fun findAllUsuarios(): List<Usuario> {
        return appDatabase.usuarioDao().findAll()
    }

    override suspend fun insertInfo(info: List<Info>) {
        return appDatabase.infoDao().addAll(info)
    }

    override suspend fun findById(id: Long): Info {
        return appDatabase.infoDao().findById(id)
    }

    override suspend fun findAllInfo(): List<Info> {
        return appDatabase.infoDao().findAll()
    }

    override suspend fun findByUsuarioId(id: Long): List<Info> {
        return appDatabase.infoDao().findByUsuarioId(id)
    }
}