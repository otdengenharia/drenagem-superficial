package com.otdengenharia.appotd.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.otdengenharia.appotd.local.entity.Info

@Dao
interface InfoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAll(info: List<Info>)

    @Query("SELECT * FROM Info")
    fun findAll(): List<Info>

    @Query("SELECT * FROM Info WHERE id = :id")
    fun findById(id: Long): Info

    @Query("SELECT * FROM Info WHERE usuario_id = :id")
    fun findByUsuarioId(id: Long): List<Info>
}