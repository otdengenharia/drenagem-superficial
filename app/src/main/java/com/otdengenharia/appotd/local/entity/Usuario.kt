package com.otdengenharia.appotd.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "Usuario")
@Parcelize
data class Usuario(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var nome: String,
    var email: String,
    var senha: String,
    @ColumnInfo(name = "nivel_acesso") var nivelAcesso: String,
    var status: String
) : Parcelable