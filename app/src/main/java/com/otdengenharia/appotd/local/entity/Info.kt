package com.otdengenharia.appotd.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "Info")
@Parcelize
data class Info(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    @ColumnInfo(name = "usuario_id") var usuarioId: Int,
    @ColumnInfo(name = "campanha_id") var campanhaId: Int,
    @ColumnInfo(name = "concessionaria_id") var concessionariaId: Int,
    @ColumnInfo(name = "nome_concessionaria") var nomeConcessionaria: String,
    var disciplina: String,
    @ColumnInfo(name = "data_inicio") var dataInicio: String,
    @ColumnInfo(name = "data_final") var dataFinal: String,
    var logo: String
    ) : Parcelable
