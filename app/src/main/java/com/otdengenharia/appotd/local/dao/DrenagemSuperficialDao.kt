package com.otdengenharia.appotd.local.dao

import androidx.room.*
import com.otdengenharia.appotd.local.entity.DrenagemSuperficial

@Dao
interface DrenagemSuperficialDao {

    @Query("SELECT * FROM DrenagemSuperficial ORDER BY km")
    fun all(): List<DrenagemSuperficial>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(vararg dispositivo: DrenagemSuperficial)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAll(dispositivos: List<DrenagemSuperficial>)

    @Update
    fun update(dispositive : DrenagemSuperficial)

    @Query("SELECT * FROM DrenagemSuperficial WHERE codAuto = :codAuto")
    fun findByCodAuto(codAuto: Long): DrenagemSuperficial

    @Query("SELECT COUNT(avaliado) FROM DrenagemSuperficial WHERE avaliado = :state")
    fun count(state: String): Int

    @Query("SELECT COUNT(avaliado) FROM DrenagemSuperficial WHERE avaliado = :state AND data = :data")
    fun countByDate(state: String, data: String): Int

    @Query("SELECT COUNT(*) FROM DrenagemSuperficial WHERE identificacao LIKE '%' || :id || '%'")
    fun verifyId(id: String): Int

    @Query("SELECT MAX(codAuto) FROM DrenagemSuperficial")
    fun getMaxCodAuto(): Long

    @Query("SELECT * FROM DrenagemSuperficial WHERE data = :date")
    fun findByDate(date: String): List<DrenagemSuperficial>

    @Query("SELECT * FROM DrenagemSuperficial WHERE segmento = :segmento")
    fun findBySegmento(segmento: Int): List<DrenagemSuperficial>
}