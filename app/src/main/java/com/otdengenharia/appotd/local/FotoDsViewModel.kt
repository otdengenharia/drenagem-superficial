package com.otdengenharia.appotd.local

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.otdengenharia.appotd.local.db.DatabaseHelper
import com.otdengenharia.appotd.local.entity.DrenagemSuperficial
import com.otdengenharia.appotd.local.entity.FotoDs
import kotlinx.coroutines.launch

class FotoDsViewModel(
    private val dbHelper: DatabaseHelper
) : ViewModel() {

    fun updateDispositiveDs(dispositive: DrenagemSuperficial) {
        viewModelScope.launch {
            dbHelper.insertDispositivesDs(dispositive)
        }
    }

    fun insertPhotoDispositiveDs(photo: FotoDs) {
        viewModelScope.launch {
            dbHelper.insertPhotosDispositivesDs(photo)
        }
    }

    fun getPhotosDispositiveDs(codAuto : Long) : List<FotoDs>? {
        var photos : List<FotoDs>? = null
        viewModelScope.launch {
           photos = dbHelper.getPhotosDispositivesDsByCodAuto(codAuto)
        }
        return photos
    }
}